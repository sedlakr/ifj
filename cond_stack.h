/*
###############################################################################
###############################################################################
########                                                               ########
########    Implementace interpretu imperativn�ho jazyka IFJ11         ########
######## ------------------------------------------------------------- ########
########    http://goo.gl/368JS                                        ########
######## ------------------------------------------------------------- ########
########   Vypracovali: Pavl�na Puncocharova  <xpunco01@fit.vutbr.cz>  ########
########                Radek Sedlak          <xsedla90@fit.vutbr.cz>  ########
########                Michal S�eptun         <xseptu00@fit.vutbr.cz>  ########
########                Martin Soulak         <xsoula02@fit.vutbr.cz>  ########
########                Jakub Vones�           <xvones02@fit.vutbr.cz>  ########
########                                                               ########
###############################################################################
###############################################################################
*/

#ifndef STACKCOND_H_
#define STACKCOND_H_


#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "ilist.h"

typedef struct StackCondItem{
        int cond_done;      // bool hodnota, ktera obsahuje vysledek podminky ifu / whilu 
        struct listItem *saveAddrActive; // adresa pri skoku ve while
        struct StackCondItem *next;
} TStackCondItem;

typedef struct StackCond{
        TStackCondItem *first;
        unsigned int count;
} TStackCond;

void initStackCond(TStackCond *s);
void pushStackCond(TStackCond *s, int cond_done, struct listItem *saveAddrActive);
void freeStackCond(TStackCond *s);
void popStackCond(TStackCond *s);
int topStackCond(TStackCond *s);
struct listItem *topStackAddr(TStackCond *s);

#endif