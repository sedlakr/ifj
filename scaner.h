/*
###############################################################################
###############################################################################
########                                                               ########
########    Implementace interpretu imperativn�ho jazyka IFJ11         ########
######## ------------------------------------------------------------- ########
########    http://goo.gl/368JS                                        ########
######## ------------------------------------------------------------- ########
########   Vypracovali: Pavl�na Puncocharova  <xpunco01@fit.vutbr.cz>  ########
########                Radek Sedlak          <xsedla90@fit.vutbr.cz>  ########
########                Michal S�eptun         <xseptu00@fit.vutbr.cz>  ########
########                Martin Soulak         <xsoula02@fit.vutbr.cz>  ########
########                Jakub Vones�           <xvones02@fit.vutbr.cz>  ########
########                                                               ########
###############################################################################
###############################################################################
*/

//hlavicka pro lexikalni analyzator
#ifndef SCANNER_H_
#define SCANNER_H_

#include <stdio.h>
#include <ctype.h>
#include "str.h"
#include "main.h"
//chybove hlasky
#define LEX_ERROR               1

#define LEXEM_BEGIN  9
//klicova slova
#define DO          10
#define ELSE        11
#define END         12
#define FALSE       13
#define FUNCTION    14
#define IF          15
#define LOCAL       16
#define NIL         17
#define READ        18
#define RETURN      19
#define THEN        20
#define TRUE        21
#define WHILE       22
#define WRITE       23

// rezervovana slova
#define AND         24
#define BREAK       25
#define ELSEIF      26
#define FOR         27
#define IN          28
#define NOT         29
#define OR          30
#define REPEAT      31
#define UNTIL       32

//jednotlive znaky
#define COMMA            40 // ','
#define LEFT_VINCULUM    41 // '{'
#define RIGHT_VINCULUM   42 // '}'
#define LEFT_BRACKET     43 // '('
#define RIGHT_BRACKET    44 // ')'
#define SEMICOLON        45 // ';'
#define PLUS             46 // '+'
#define MULTIPLE         47 // '*'
#define SLASH            48 // '/'
#define CARET            49 // '^'
#define GTR              50 // '>'
#define GTR_EQL          51 // '>='
#define LESS             52 // '<'
#define LESS_EQL         53 // '<='
#define EQL              54 // '='
#define EQUIV            55 // '=='
#define NON_EQUIV        56 // '~='
#define DOT              57 // '.'
#define CONCAT           58 // '..'
#define DASH             59 // '-'

//specialni znaky
#define END_OF_FILE      70
#define END_OF_LINE      71

// literaly
#define ID       80
#define NUM      81
#define STR      82

// stavy
#define S_INIT          0
#define S_ID            1
#define S_STR           2
#define S_ESC           3
#define S_NUMBER        4
#define S_DBL_DOT       5
#define S_DBL           6
#define S_EXP           7
#define S_EXP_SGN       8
#define S_EXP_NUM       9
#define S_DASH          10
#define S_DBL_DASH      11
#define S_LSQR_BRACK    12
#define S_RSQR_BRACK    13
#define S_LN_COM        14
#define S_BLOCK_COM     15
#define S_GTR           16
#define S_LESS          17
#define S_EQL           18
#define S_NON_EQUIV     19
#define S_CONCAT        20
#define S_EOF           21



//hlavicka funkce simulujici lexikalni analyzator
void setSourceFile(FILE *f);
int getNextToken(string *attr);
#endif
