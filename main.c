/*
###############################################################################
###############################################################################
########                                                               ########
########    Implementace interpretu imperativn�ho jazyka IFJ11         ########
######## ------------------------------------------------------------- ########
########    http://goo.gl/368JS                                        ########
######## ------------------------------------------------------------- ########
########   Vypracovali: Pavl�na Puncocharova  <xpunco01@fit.vutbr.cz>  ########
########                Radek Sedlak          <xsedla90@fit.vutbr.cz>  ########
########                Michal S�eptun         <xseptu00@fit.vutbr.cz>  ########
########                Martin Soulak         <xsoula02@fit.vutbr.cz>  ########
########                Jakub Vones�           <xvones02@fit.vutbr.cz>  ########
########                                                               ########
###############################################################################
###############################################################################
*/

#include <stdio.h>
#include "str.h"
#include "ial.h"
#include "ilist.h"
#include "scaner.h"
#include "parser.h"
#include "interpret.h"
#include "tlist.h"



int main(int argc, char** argv){
   FILE *f;
   if (argc == 1|| argc > 2){
      return OTHER_ERROR;
   }
   if ((f = fopen(argv[1], "r")) == NULL){
      return OTHER_ERROR;
   }
   setSourceFile(f);

   tSymbolTable ST;
   initHT(&ST); // inicializace tabulky symbolu

   tListOfInstr instrList;
   InitList(&instrList); // inicializace seznamu instrukci

   int result;
   result = parse(&ST, &instrList); // provedeme syntaktickou analyzu

    //toto osetreni je spatne
    // bude se muset posefovat kde dealokovat tabulku, seznam
    // nebude tady interpret error
   switch (result){
     case LEX_ERROR:
        if(DEBUG)printf("lex_error\n");
        return result;
     break;
     case SYNTAX_ERROR:
        if(DEBUG)printf("syntax_error\n");
        return result;
     break;
     case SEM_ERROR:
        if(DEBUG)printf("sem_error\n");
       // nastala chyba v prubehu prekladu
       /*tableFree(&ST);
       listFree(&instrList);
       fclose(f);
       return -result;*/
       return result;
     break;
     case INTERPRET_ERROR:
        if(DEBUG)printf("interpret_error\n");
        return result;
     break;
     case OTHER_ERROR:
        if(DEBUG)printf("other_error\n");
        return result;
     break;
     default:
        if(DEBUG)printf("parsovani a semantika probehla spravne\n");
     break;
   }
   if(DEBUG)printf("--------------------------------------------\nvypisuji tabulku symbolu + seznam instrukci\n");
   if(DEBUG)PrintHT(&ST);
   if(DEBUG)printIList(&instrList);
   if(DEBUG)printf("--------------------------------------------\nkonec vypisu tabulky symbolu + seznamu instrukci\n");
   // provedeme interpretaci
   result=interpret(&ST, &instrList);
   if(result!=OK) {if(DEBUG)printf("interpret error\n");}
    //dealokace tabulky symbolu a listu instrukci
   destroyHT(&ST);
   DisposeList(&instrList);
   fclose(f);
   return result;
}
