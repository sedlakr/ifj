/*
###############################################################################
###############################################################################
########                                                               ########
########    Implementace interpretu imperativn�ho jazyka IFJ11         ########
######## ------------------------------------------------------------- ########
########    http://goo.gl/368JS                                        ########
######## ------------------------------------------------------------- ########
########   Vypracovali: Pavl�na Puncocharova  <xpunco01@fit.vutbr.cz>  ########
########                Radek Sedlak          <xsedla90@fit.vutbr.cz>  ########
########                Michal S�eptun         <xseptu00@fit.vutbr.cz>  ########
########                Martin Soulak         <xsoula02@fit.vutbr.cz>  ########
########                Jakub Vones�           <xvones02@fit.vutbr.cz>  ########
########                                                               ########
###############################################################################
###############################################################################
*/

#ifndef TLIST_H_
#define TLIST_H_

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "ial.h"
#include "str.h"

typedef struct SETLIST{
        struct TLIST *first;
        struct TLIST *last;
} SetList;

typedef struct TLIST{
        string funkce;               // nazev funkce
        tTableItem table;      // ukazatel na ukazatel, pro navrat hodnotu
        void *instr_adr;
        int count_params;             // po�et parametr�
        struct PARAM_ITEM *first;     // pointer na prvni parametr
        struct PARAM_ITEM *last;      // pointer na posledni parametr
        struct PARAM_ITEM *actual;    // pointer na aktualni parametr
        struct TLIST *next;           // pointer na dalsi TLIST
} TList;


typedef struct PARAM_ITEM{
         void *adr;                     // adresa
         struct PARAM_ITEM *next;       // pointer na dalsi PARAM_ITEM
} ParamItem;

/* - FUNKCE - */
void setList(SetList *L);
void initTList(TList *L);
int InsertLast_TList(SetList *L, string *funkce, tTableItem table,void *instr_adr);
int InsertLast_ParamItem(TList *L, void *adr);
ParamItem *readParam_TList(TList *L, ParamItem *aktual);
TList *findTList(SetList *SL, string *s);
void freeSetList(SetList *L);
void view_TList();

#endif
