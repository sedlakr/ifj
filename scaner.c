/*
###############################################################################
###############################################################################
########                                                               ########
########    Implementace interpretu imperativn�ho jazyka IFJ11         ########
######## ------------------------------------------------------------- ########
########    http://goo.gl/368JS                                        ########
######## ------------------------------------------------------------- ########
########   Vypracovali: Pavl�na Puncocharova  <xpunco01@fit.vutbr.cz>  ########
########                Radek Sedlak          <xsedla90@fit.vutbr.cz>  ########
########                Michal S�eptun         <xseptu00@fit.vutbr.cz>  ########
########                Martin Soulak         <xsoula02@fit.vutbr.cz>  ########
########                Jakub Vones�           <xvones02@fit.vutbr.cz>  ########
########                                                               ########
###############################################################################
###############################################################################
*/

// lexikalni analyzator - scaner
#include <stdio.h>
#include <ctype.h>
#include "str.h"
#include "scaner.h"

// promenna pro ulozeni vstupniho souboru
FILE *source;

void setSourceFile(FILE *f)
{
  source = f;
}

int getNextToken(string *attr)

{
   int state= 0;
   int c;

   // vymazat predchozi atribut
   if(strClear(attr)!=OK) return OTHER_ERROR;

   while (1){
     // nacteni dalsiho znaku
     c = getc(source);

     switch (state){
        /* state = INIT */
        case S_INIT:
            // prazdna mista
            if(isspace(c)) state = S_INIT;
            else if (c == EOF) {
                strClear(attr);
                return END_OF_FILE;
            }
            // identifikatory
            else if((isalpha(c)) || c == 95){
                strAddChar(attr, c);
                state = S_ID;
            }
            // cisla
            else if((isdigit(c))){
                strAddChar(attr, c);
                state = S_NUMBER;
            }
            else if(c == 34){  // string, 34 - "
                state = S_STR;
            }
            else if(c == '-'){  // - nebo line comment
                state = S_DASH;
            }
            else if(c == 44) return COMMA;  //,
            else if(c == 123) return LEFT_VINCULUM; //{
            else if(c == 125) return RIGHT_VINCULUM; //}
            else if(c == 40) return LEFT_BRACKET; //(
            else if(c == 41) return RIGHT_BRACKET; //)
            else if(c == 59) return SEMICOLON; //;
            else if(c == 43) return PLUS; //+
            else if(c == 42) return MULTIPLE; //*
            else if(c == 47) return SLASH; // /
            else if(c == 94) return CARET; //^
            else if(c == 62){ // >
                state = S_GTR;
            }
            else if(c == 60) { //<
                state = S_LESS;
            }
            else if(c == 61) { //=
                state = S_EQL;
            }
            else if(c == 126) { //~
                strAddChar(attr, c);
                state = S_NON_EQUIV;
            }
            else if(c == 46) { //.
                strAddChar(attr, c);
                state = S_CONCAT;
            }
            else return LEX_ERROR;
            break;

        /* state = ID */
        case S_ID:
            //nacitani identifikatoru, 95 -> podtrz
            if(isalnum(c) || c == 95) strAddChar(attr, c);
            else{

                ungetc(c, source);

                // kontrola identifikatoru
                int checkKeyw = checkKeywords(attr);

                // pokud checkKeywords vrati -1 jedna se o identifikator, jinak se vraci cislo klicoveho slova viz. scaner.h
                if(checkKeyw == -1){
                                return ID;
                }
                else return checkKeyw;
            }
            break;

        // cislo zacina
        case S_NUMBER:
            //if(c == EOF) return END_OF_FILE;
            if(isdigit(c)) strAddChar(attr, c);
            else if(c == '.'){
                strAddChar(attr, c);
                state = S_DBL_DOT; // double
            }
            else if(c == 'e' || c == 'E'){
                strAddChar(attr, c);
                state = S_EXP; // exponent
            }
            else{
               ungetc(c, source);
               return NUM;
            }
            break;

        //prisla desetinna tecka
        case S_DBL_DOT:
            if(c == EOF) state = S_EOF;
            else if(isdigit(c)){
                strAddChar(attr, c);
                state = S_DBL;
            }
            else return LEX_ERROR;
            break;

        // prislo desetinne misto
        case S_DBL:
            if(c == EOF) state = S_EOF;
            else if(isdigit(c)) strAddChar(attr, c);
            else if(c == 'e' || c == 'E'){
                strAddChar(attr, c);
                state = S_EXP; // exponent
            }
            else{
               ungetc(c, source);
               return NUM;
            }
            break;

        // prisel exponent
        case S_EXP:
            if(isdigit(c)){
                strAddChar(attr, c);
                state = S_EXP_NUM;
            }
            else if(c == 43 || c == 45){
                strAddChar(attr, c);
                state = S_EXP_SGN; // exponent se znamenkem

            }
            else return LEX_ERROR;
            break;

        // znamenko exponentu
        case S_EXP_SGN:
            if(isdigit(c)){
                strAddChar(attr, c);
                state = S_EXP_NUM;
            }
            else return LEX_ERROR;
            break;

        // hodnota exponentu
        case S_EXP_NUM:
            if(isdigit(c)){
                strAddChar(attr, c);
                state = S_EXP_NUM;
            }
            else{
                ungetc(c, source);
                return NUM;
            }
            break;

        // string
        case S_STR:
            if(c == EOF) return LEX_ERROR;
            else if (c == 34) {
                return STR;
            }
            else if (c == 92){ // 92 - zpetne lomitko
                state = S_ESC;
                }
            else strAddChar(attr, c);
            break;

        // escape sekvence
        case S_ESC:
            if(c == 'n'){
                strAddChar(attr, '\n');
                state = S_STR;
            }
            else if(c == 't'){
                strAddChar(attr, '\t');
                state = S_STR;
            }
            else if(c == 92){
                strAddChar(attr, 92); // backslash
                state = S_STR;
            }
            else if(c == 34){
                strAddChar(attr, '"');
                state = S_STR;
            }
            
            // do esc sekvence prislo 1.cislo
            else if(isdigit(c)){

                    int stovky = (c - '0')*100;

                    c = getc(source);
                    if(isdigit(c)){
                        int desitky = (c - '0')*10;

                        c = getc(source);
                        if(isdigit(c)){
                            int jednotky = (c - '0');

                            int vysledek = stovky + desitky + jednotky;
                            if(vysledek >= 0 && vysledek <=255){
                             strAddChar(attr,vysledek);
                             state = S_STR;
                            }
                            else return LEX_ERROR;
                        }
                        else return LEX_ERROR;
                    }
                    else return LEX_ERROR;

            }
            else return LEX_ERROR;
            break;

        // dash
        case S_DASH:    // 45 - -
            if (c == 45) state = S_DBL_DASH;
            else {
                ungetc(c, source);
                return DASH;
            }
            break;

        // double dash
        case S_DBL_DASH:
            if(c == EOF) state = S_EOF;
            else if (c == 91) state = S_LSQR_BRACK; // 91 - [
            else state = S_LN_COM;
            break;

        // line comment
        case S_LN_COM:
            if(c == EOF) state = S_EOF;
            else if(c == 10){
                strClear(attr);
                state = S_INIT;
            }
            else state = S_LN_COM;
            break;

        // leva hranata
        case S_LSQR_BRACK:
            if(c == EOF) state = S_EOF;
            else if(c == 91) state = S_BLOCK_COM; // 91 - [
            else state = S_LN_COM;
            break;

        // block comment
        case S_BLOCK_COM:
            if(c == EOF) state = S_EOF;
            else if(c == 93) state = S_RSQR_BRACK; // 93 - ]
            else state = S_BLOCK_COM;
            break;

        // prava hranata
        case S_RSQR_BRACK:
            if(c == EOF) state = S_EOF;
            else if(c == 93) {
                state = S_INIT; // 93 - ]
                strClear(attr);
            }
            else state = S_BLOCK_COM;
            break;

        // vetsi
        case S_GTR: // 61 - =
            if(c == 61) {
                strAddChar(attr, c);
                return GTR_EQL;
            }
            else {
                ungetc(c, source);
                return GTR;
            }
            break;

        // mensi
        case S_LESS: // 61 - =
            if(c == 61) {
                strAddChar(attr, c);
                return LESS_EQL;
            }
            else {
                ungetc(c, source);
                return LESS;
            }
            break;

        // rovno
        case S_EQL: // 61 - =
            if(c == 61) {
                strAddChar(attr, c);
                return EQUIV;
            }
            else {
                ungetc(c, source);
                return EQL;
            }
            break;

        // nerovno vlnka
        case S_NON_EQUIV: // 61 - =
            if(c == 61) {
                strAddChar(attr, c);
                return NON_EQUIV;
            }
            else return LEX_ERROR;
            break;

        // konkatenace
        case S_CONCAT: // 46 - .
            if(c == 46) {
                strAddChar(attr, c);
                return CONCAT;
            }
            else {
                ungetc(c, source);
                return DOT;
            }
            break;
        case S_EOF:
            strClear(attr);
            return END_OF_FILE;

        // cokoliv jineho -> ERROR
        default:
            return LEX_ERROR;

    }
  }
}
