/*
###############################################################################
###############################################################################
########                                                               ########
########    Implementace interpretu imperativn�ho jazyka IFJ11         ########
######## ------------------------------------------------------------- ########
########    http://goo.gl/368JS                                        ########
######## ------------------------------------------------------------- ########
########   Vypracovali: Pavl�na Puncocharova  <xpunco01@fit.vutbr.cz>  ########
########                Radek Sedlak          <xsedla90@fit.vutbr.cz>  ########
########                Michal S�eptun         <xseptu00@fit.vutbr.cz>  ########
########                Martin Soulak         <xsoula02@fit.vutbr.cz>  ########
########                Jakub Vones�           <xvones02@fit.vutbr.cz>  ########
########                                                               ########
###############################################################################
###############################################################################
*/

// seznam instrukci a operace nad nim ala Honzik

#include <stdio.h>
#include <stdlib.h>
#include "ilist.h"
#define DEBUG 0

void InitList (tListOfInstr *L){
// funkce inicializuje seznam instrukci
  L->First  = NULL;
  L->Last   = NULL;
  L->Active = NULL;
}

void DisposeList (tListOfInstr *L){
// funkce dealokuje seznam instrukci

  tListItem *temp;

  while (L->First != NULL){
    temp = L->First;
    L->First = L->First->Ptr;
    // uvolnime celou polozku
    free(temp);
  }

  if(L->Active != NULL) L->Active = NULL;

}

int ListInsertLast(tListOfInstr *L, tInstr I){
// vlozi novou instruci na konec seznamu

  tListItem *newItem;
  newItem = malloc(sizeof (tListItem));
  if(newItem == NULL) return 5;

  newItem->Instruction = I;
  newItem->Ptr = NULL;
  if (L->First == NULL) L->First = newItem;
  else L->Last->Ptr = newItem;

  L->Last=newItem;
  return ILIST_OK;
}

void First(tListOfInstr *L){
// zaktivuje prvni instrukci

  L->Active = L->First;
  if(DEBUG){printf("Lactive=Lfirst > typ=%d\n",L->Active->Instruction.instType);}
}

void Succ(tListOfInstr *L){
// aktivni instrukci se stane nasledujici instrukce

      if(L->Active != NULL)
        L->Active = L->Active->Ptr;

}

void listJumpTo(tListOfInstr *L, void * jumpToInst){
// nastavi aktivni na adresu v parametru jumpToInst
  L->Active = (tListItem*) jumpToInst;
}

void *listGetPointerLast(tListOfInstr *L){
// vrati ukazatel na posledni instrukci
// POZOR, z hlediska predmetu IAL tato funkce narusuje strukturu
// abstraktniho datoveho typu

  return (void*) L->Last;
}

tInstr *ListCopyActive(tListOfInstr *L){
// vrati aktivni instrukci

  if (L->Active == NULL){
    return NULL;
  }
  else return &(L->Active->Instruction);
}


void printIList(tListOfInstr *L){
    printf("--------print ILIST ---------\n");
    tListItem *hlp=L->First;
    while(hlp != NULL){
            printf("\n\t %d",hlp->Instruction.instType);
            if(hlp==L->Active) printf(" --active");
            printf("\n");
            hlp=hlp->Ptr;
        }
    printf("--------print ILIST END---------\n");
}
