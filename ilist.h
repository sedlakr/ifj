/*
###############################################################################
###############################################################################
########                                                               ########
########    Implementace interpretu imperativn�ho jazyka IFJ11         ########
######## ------------------------------------------------------------- ########
########    http://goo.gl/368JS                                        ########
######## ------------------------------------------------------------- ########
########   Vypracovali: Pavl�na Puncocharova  <xpunco01@fit.vutbr.cz>  ########
########                Radek Sedlak          <xsedla90@fit.vutbr.cz>  ########
########                Michal S�eptun         <xseptu00@fit.vutbr.cz>  ########
########                Martin Soulak         <xsoula02@fit.vutbr.cz>  ########
########                Jakub Vones�           <xvones02@fit.vutbr.cz>  ########
########                                                               ########
###############################################################################
###############################################################################
*/

#ifndef ILIST_H_
#define ILIST_H_

#define ILIST_OK      0

//------- typy jednotlivych instrukci -------//

// prirazeni
#define I_SETNULL           1   // a = 0
#define I_SETVALUE_NUM      2   // a = 6
#define I_SETVALUE_STR      3   // a = "ahoj"
#define I_SETVALUE_BOOL     4   // a = true
#define I_SETVAR            5   // a = b

// dilci funkce
//------ Cteni ze vstupu (scanf) ------//
#define I_READ_NUM          6   // nacte cislo
#define I_READ_STR_EOL      7   // nacte string po EOL
#define I_READ_STR_EOF      8   // nacte string po EOF
#define I_READ_STR_CNT      9   // nacte zadany pocet znaku

//------ Vypis (printf) -------//
#define I_WRITE_STR     10
#define I_WRITE_NUM     11
#define I_WRITE_VAR     12

#define I_TYPE          14
#define I_SUBSTR        15
#define I_FIND          16
#define I_SORT          17
#define I_CONCAT        18
#define I_RETURN        19

#define I_FUNCTION      20
#define I_FUNC_START    21
#define I_FUNC_END      22
#define I_FUNC_JMP      23

#define I_ADD           34
#define I_SUB           35
#define I_MUL           36
#define I_DIV           37
#define I_POWER         38

#define I_JUMP_TO       39

#define I_EQUIV         40 // ==
#define I_GTR           41 // >
#define I_GTR_EQL       42 // >=
#define I_LESS          43 // <
#define I_LESS_EQL      44 // <=
#define I_NON_EQUIV     45 // ~=

// instrukce pro IF
#define I_IF            50
#define I_IF_END        51
#define I_ELSE          52
#define I_END           53

// instrukce pro WHILE
#define I_WHILE_START   54
#define I_WHILE_COND    55
#define I_WHILE_END     56
#define I_WHILE         57


//instrukce konce programu
#define I_STOP          120

typedef struct{
  int instType;  // typ instrukce
  void *addr1; // adresa 1
  void *addr2; // adresa 2
  void *addr3; // adresa 3
} tInstr;

typedef struct listItem{
  tInstr Instruction;
  struct listItem *Ptr;
} tListItem;

typedef struct{
  struct listItem *First;  // ukazatel na prvni prvek
  struct listItem *Last;   // ukazatel na posledni prvek
  struct listItem *Active; // ukazatel na aktivni prvek
} tListOfInstr;

void InitList(tListOfInstr *L);
void DisposeList(tListOfInstr *L);
int ListInsertLast(tListOfInstr *L, tInstr I);
void First(tListOfInstr *L);
void Succ(tListOfInstr *L);
void listJumpTo(tListOfInstr *L, void * jumpToInst);
void *listGetPointerLast(tListOfInstr *L);
tInstr *ListCopyActive(tListOfInstr *L);
void printIList(tListOfInstr *L);

#endif
