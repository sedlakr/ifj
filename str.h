/*
###############################################################################
###############################################################################
########                                                               ########
########    Implementace interpretu imperativn�ho jazyka IFJ11         ########
######## ------------------------------------------------------------- ########
########    http://goo.gl/368JS                                        ########
######## ------------------------------------------------------------- ########
########   Vypracovali: Pavl�na Puncocharova  <xpunco01@fit.vutbr.cz>  ########
########                Radek Sedlak          <xsedla90@fit.vutbr.cz>  ########
########                Michal S�eptun         <xseptu00@fit.vutbr.cz>  ########
########                Martin Soulak         <xsoula02@fit.vutbr.cz>  ########
########                Jakub Vones�           <xvones02@fit.vutbr.cz>  ########
########                                                               ########
###############################################################################
###############################################################################
*/

//hlavickovy soubor pro praci s nekonecne dlouhymi retezci
#ifndef STR_H_
#define STR_H_

#include <stdbool.h>

typedef struct
{
  char* str;        // misto pro dany retezec ukonceny znakem '\0'
  int length;        // skutecna delka retezce
  int allocSize;    // velikost alokovane pameti
} string;

void strFree(string *s);
int strInit(string *s);
int strClear(string *s);
int strAddChar(string *s1, char c);
int checkKeywords(string *attr);
char *valueString(string *str);
int lenghtString(string *str);
bool empty(string *string);
bool copyString(string *cil_string, string *string);
bool initString(string *str);
string substr(string *str, int start, int konec);
bool LexCompare(int com, string *operand_1, string *operand_2);
#endif
