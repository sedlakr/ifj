/*
###############################################################################
###############################################################################
########                                                               ########
########    Implementace interpretu imperativn�ho jazyka IFJ11         ########
######## ------------------------------------------------------------- ########
########    http://goo.gl/368JS                                        ########
######## ------------------------------------------------------------- ########
########   Vypracovali: Pavl�na Puncocharova  <xpunco01@fit.vutbr.cz>  ########
########                Radek Sedlak          <xsedla90@fit.vutbr.cz>  ########
########                Michal S�eptun         <xseptu00@fit.vutbr.cz>  ########
########                Martin Soulak         <xsoula02@fit.vutbr.cz>  ########
########                Jakub Vones�           <xvones02@fit.vutbr.cz>  ########
########                                                               ########
###############################################################################
###############################################################################
*/
 
#ifndef STACK_H_
#define STACK_H_

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "ial.h"

typedef struct StackItem{
        int token;
        struct TableItem *table;
        struct StackItem *next;
}TStackItem;

typedef struct Stack{
        TStackItem *first;
        unsigned int count;
}TStack;

void initStack(TStack *stack);
TStackItem *allocItem_Stack();  
void saveItem(TStackItem *n_item, int token, tTableItem table);
bool addItem_Stack(TStack *n_stack, int token, tTableItem table);   
bool addStSRule_Stack(TStack *n_stack, int pos);
TStackItem *returnFirst_Item(TStack *stack);
TStackItem *freeItem_Stack(TStack *stack, TStackItem *f_item);
void freeStack(TStack *stack);
void freeFirst(TStack *stack);
void freeXItem(TStack *stack, int count);
void viewStack(TStack *stack);

#endif
