/*
###############################################################################
###############################################################################
########                                                               ########
########    Implementace interpretu imperativn�ho jazyka IFJ11         ########
######## ------------------------------------------------------------- ########
########    http://goo.gl/368JS                                        ########
######## ------------------------------------------------------------- ########
########   Vypracovali: Pavl�na Puncocharova  <xpunco01@fit.vutbr.cz>  ########
########                Radek Sedlak          <xsedla90@fit.vutbr.cz>  ########
########                Michal S�eptun         <xseptu00@fit.vutbr.cz>  ########
########                Martin Soulak         <xsoula02@fit.vutbr.cz>  ########
########                Jakub Vones�           <xvones02@fit.vutbr.cz>  ########
########                                                               ########
###############################################################################
###############################################################################
*/

#include "tlist.h"

// inicializuje seznam
void setList(SetList *L){
     if(L != NULL){
          L->first = NULL;
          L->last  = NULL;
     }
}

// inicializuje TList
void initTList(TList *L){
     if(L != NULL){
          strInit(&(L->funkce));
          L->table  = NULL;
          L->instr_adr = NULL;
          L->count_params = 0;
          L->first  = NULL;
          L->last   = NULL;
          L->actual = NULL;
          L->next   = NULL;
     }
}

// inicializuje parametr
void initParamItem(ParamItem *P){
     if(P != NULL){
          P->next = NULL;
          P->adr  = NULL;
     }
}

// vlozi polozku seznamu a posledni misto
int InsertLast_TList(SetList *L, string *funkce, tTableItem table,void *instr_adr){
     TList *new_list = (TList*)malloc(sizeof(TList));
     // vraceni chybove konstanty c. 5
     if(new_list == NULL) return 5;
     initTList(new_list);
     if (L->first == NULL) L->first = new_list;
     else L->last->next = new_list;
     L->last = new_list;
     // nastaveni hodnot
     copyString(&(new_list->funkce),funkce);
     new_list->table = table;
     new_list->instr_adr = instr_adr;
     return 0;
}

// vlozi parametr na konec seznamu
int InsertLast_ParamItem(TList *L, void *adr){
    ParamItem *new_param = (ParamItem*)malloc(sizeof(ParamItem));
    // vraceni chybove konstanty c. 5
    if(new_param == NULL) return 5;

    initParamItem(new_param);

    if (L->first == NULL)L->first = new_param;
    else L->last->next = new_param;
    L->last = new_param;

    // nastaveni hodnot
    L->count_params++;
    new_param->adr = adr;
    return 0;
}

// uvolneni obou seznamu
void freeSetList(SetList *L){
    if(L != NULL){
         TList *pomc;
         TList *item = L->first;
         while(item != NULL){
               pomc = item;
               if(pomc->first != NULL){
                      // podseznam
                      ParamItem *param = pomc->first;
                      ParamItem *p_pom;
                      while(param != NULL){
                           p_pom = param;
                           param = param->next;
                           free(p_pom);
                      }
               }
               item = item->next;
               strFree(&(pomc->funkce));
               free(pomc);
         }
         setList(L);
    }
}

// vrati aktualni Parametr a aktualni nastavi na dalsi
ParamItem *readParam_TList(TList *L, ParamItem *aktual){
    if(aktual == NULL){
              if((aktual = L->first) == NULL)
                         return NULL;
    }
    ParamItem *read = aktual;
    L->actual = aktual->next;
    return read;
}

// Vyhled� TList pomoc� TList->funkce a string s
TList *findTList(SetList *SL, string *s){
    if(SL != NULL){
        TList *item = SL->first;
        while(item != NULL){
               if(strcmp(item->funkce.str, s->str) == 0){
                   return item;
               }
               item = item->next;
        }
        return NULL;
    }
    return NULL;
}

void view_TList(SetList *L){
     if(L != NULL){
         TList *item = L->first;
         printf("---printing TLIST---\n");
         while(item != NULL){

              printf("count_params :%d ",item->count_params);
              printf("instr_addr :%d ",(int)item->instr_adr);
              printf("func name: %s\n", item->funkce.str);
              ParamItem *param = item->first;
              while(param != NULL){
                   printf(" - %d \n", (int)param->adr);
                   param = param->next;
              }
              item = item->next;
         }
         printf("---end of printing TLIST---\n");
         printf("\n\n");
     }
}



