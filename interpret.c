/*
###############################################################################
###############################################################################
########                                                               ########
########    Implementace interpretu imperativn�ho jazyka IFJ11         ########
######## ------------------------------------------------------------- ########
########    http://goo.gl/368JS                                        ########
######## ------------------------------------------------------------- ########
########   Vypracovali: Pavl�na Puncocharova  <xpunco01@fit.vutbr.cz>  ########
########                Radek Sedlak          <xsedla90@fit.vutbr.cz>  ########
########                Michal S�eptun         <xseptu00@fit.vutbr.cz>  ########
########                Martin Soulak         <xsoula02@fit.vutbr.cz>  ########
########                Jakub Vones�           <xvones02@fit.vutbr.cz>  ########
########                                                               ########
###############################################################################
###############################################################################
*/

#include <stdio.h>
#include <math.h>
#include "str.h"
#include "ial.h"
#include "ilist.h"
#include "interpret.h"
#include "cond_stack.h"

int interpret(tSymbolTable *ST, tListOfInstr *instrList){

  tInstr *I;

  // pomocne promenne pro I_SUBSTR
  bool substr_cont = false;
  tTableItem output_ptr = NULL;

  // pomocne promenne pro cykly
  TStackCond SCond; // zasobnik pro true/false pro splneni podminky v ifu
  TStackCond SRank; // poradi zanoreni

  TStackCond SAddr; // zasobnik adres pro while
  TStackCond SCondWhile; // zasobnik pro true/false pro splneni podminky ve whilu
  TStackCond SRankWhile; // poradi zanoreni

  // pomocne promenne pro uzivatelske funkce
  TStackCond SAddrAct;  // zasobnik pro adresu aktivni instrukce

  // inicializace zasobniku
  initStackCond(&SCond);
  initStackCond(&SRank);
  initStackCond(&SAddr);
  initStackCond(&SCondWhile);
  initStackCond(&SRankWhile);
  initStackCond(&SAddrAct);

  if(DEBUG)printf("interpret ready\n");
  if(DEBUG)printIList(instrList);


 while (1){
    I = ListCopyActive(instrList);
    if(I->instType == I_STOP) return 0;

    switch (I->instType){

        case I_TYPE:{
        /*****  (I_TYPE, adr1, adr2, NULL)   *****/
        // adr1 - ulozeni typu promenne adr2
        // adr2 - promenna, jejiz typ zjistujeme
        
                string input;
                strInit(&input);
                char *inputPole;

                // hodnota je typu number
                if((((tTableItem)(I->addr2))->type) == Tnum){
                    inputPole = "number";

                    for(unsigned int i = 0; i<strlen(inputPole);i++){
                        strAddChar(&input,inputPole[i]);
                    }

                    copyString(&(((tTableItem)(I->addr1))->value.Vstr),&input);
                    ((tTableItem)(I->addr1))->type = Tstr;
                }
                // hodnota je typu string
                else if((((tTableItem)(I->addr2))->type) == Tstr){
                    inputPole = "string";

                    for(unsigned int i = 0; i<strlen(inputPole);i++){
                        strAddChar(&input,inputPole[i]);
                    }
                    if((((tTableItem)(I->addr1))->type)==Tundef){
                        strInit(&(((tTableItem)(I->addr1))->value.Vstr));
                    }
                    copyString(&(((tTableItem)(I->addr1))->value.Vstr),&input);
                    ((tTableItem)(I->addr1))->type = Tstr;
                }
                // hodnota je typu boolean
                else if((((tTableItem)(I->addr2))->type) == Tbool){
                    inputPole = "boolean";

                    for(unsigned int i = 0; i<strlen(inputPole);i++){
                        strAddChar(&input,inputPole[i]);
                    }

                    copyString(&(((tTableItem)(I->addr1))->value.Vstr),&input);
                    ((tTableItem)(I->addr1))->type = Tstr;
                }
                // hodnota je typu nil
                else {
                    inputPole = "nil";

                    for(unsigned int i = 0; i<strlen(inputPole);i++){
                        strAddChar(&input,inputPole[i]);
                    }

                    copyString(&(((tTableItem)(I->addr1))->value.Vstr),&input);
                    ((tTableItem)(I->addr1))->type = Tstr;
                }

                strFree(&input);
            break;
        }

//-------------------------------------------------------------//

        case I_SETVALUE_NUM:{
        /*****  (I_SETVALUE_NUM, adr1, adr2, NULL)   *****/
        // adr1 - promenn� do, ktere prirazujeme
        // adr2 - prirazovana hodnota
        // napr. a = 5;

                (((tTableItem)(I->addr1))->value.Vnum) = (((tTableItem)(I->addr2))->value.Vnum);
                (((tTableItem)(I->addr1))->type) = (((tTableItem)(I->addr2))->type);
            break;
        }

        case I_SETVALUE_STR:{
        /*****  (I_SETVALUE_STR, adr1, adr2, NULL)   *****/
        // adr1 - promenn� do, ktere prirazujeme
        // adr2 - prirazovana hodnota
        // napr. a = "string";
        
                if(((tTableItem)I->addr1)->type==Tundef){
                    strInit(&(((tTableItem)I->addr1)->value.Vstr));
                }
                copyString(&(((tTableItem)I->addr1)->value.Vstr), &(((tTableItem)I->addr2)->value.Vstr));
                (((tTableItem)(I->addr1))->type) = (((tTableItem)(I->addr2))->type);
            break;
        }

        case I_SETVALUE_BOOL:{
        /*****  (I_SETVALUE_BOOL, adr1, adr2, NULL)   *****/
        // adr1 - promenn� do, ktere prirazujeme
        // adr2 - prirazovana hodnota
        // napr. a = true;

                (((tTableItem)(I->addr1))->value.Vbool) = (((tTableItem)(I->addr2))->value.Vbool);
                (((tTableItem)(I->addr1))->type) = Tbool;
            break;
        }

        case I_SETVAR:{
        /*****  (I_SETVAR, adr1, adr2, NULL)   *****/
        // adr1 - promenna, do ktere prirazujeme
        // adr2 - prirazovana promenna
        // b = 5 || b = "string" || b = true
        // napr. a = b;

                // prirazovana hodnota je typu string
                if((((tTableItem)(I->addr2))->type) == Tstr){
                    if(((tTableItem)I->addr1)->type!=Tstr){
                        strInit(&(((tTableItem)I->addr1)->value.Vstr));
                    }
                    ((tTableItem)(I->addr1))->type = Tstr;  // nastaveni typu vysledku
                    copyString(&(((tTableItem)I->addr1)->value.Vstr), &(((tTableItem)I->addr2)->value.Vstr));   // zkopirovani retezce z promenne do vysledku
                }

                // prirazovana hodnota je typu boolean
                if((((tTableItem)(I->addr2))->type) == Tbool){
                    if(((tTableItem)I->addr1)->type==Tstr){
                        strFree(&(((tTableItem)I->addr1)->value.Vstr));
                    }
                    ((tTableItem)(I->addr1))->type = Tbool;  // nastaveni typu vysledku
                    (((tTableItem)(I->addr1))->value.Vbool) = (((tTableItem)(I->addr2))->value.Vbool);  // nastaveni hodnoty vysledku
                }

                // prirazovana hodnota je typu number
                if((((tTableItem)(I->addr2))->type) == Tnum){
                    if(((tTableItem)I->addr1)->type==Tstr){
                        strFree(&(((tTableItem)I->addr1)->value.Vstr));
                    }
                    ((tTableItem)(I->addr1))->type = Tnum;  // nastaveni typu vysledku
                    (((tTableItem)(I->addr1))->value.Vnum) = (((tTableItem)(I->addr2))->value.Vnum);    // nastaveni hodnoty vysledku
                }

                // prirazovana hodnota je typu nil
                if((((tTableItem)(I->addr2))->type) == Tundef){
                    if(((tTableItem)I->addr1)->type==Tstr){
                        strFree(&(((tTableItem)I->addr1)->value.Vstr));
                    }
                    ((tTableItem)(I->addr1))->type = Tundef;  // nastaveni typu vysledku
                }
            break;
        }

//--------------------------- OPERACE S CISLY ----------------------------------//

        case I_ADD:{
        /*****  (I_ADD, adr1, adr2, adr3)   *****/
        // adr1 - sou�et
        // adr2, adr3 - s��tance
        // adr1 = adr2 + adr3

                // osetreni moznych chybovych stavu interpretu
                if((((tTableItem)(I->addr2))->type) != Tnum) return INTERPRET_ERROR;
                if((((tTableItem)(I->addr3))->type) != Tnum) return INTERPRET_ERROR;

                // samotna operace a nastaveni typu vysledku
                (((tTableItem)(I->addr1))->value.Vnum) = (((tTableItem)(I->addr2))->value.Vnum) + (((tTableItem)(I->addr3))->value.Vnum);
                ((tTableItem)(I->addr1))->type = Tnum;
            break;
        }

        case I_SUB:{
        /*****  (I_SUB, adr1, adr2, adr3)   *****/
        // adr1 - rozdil
        // adr2, adr3 - mensenec, mensitel
        // adr1 = adr2 - adr3

                // osetreni moznych chybovych stavu interpretu
                if((((tTableItem)(I->addr2))->type) != Tnum) return INTERPRET_ERROR;
                if((((tTableItem)(I->addr3))->type) != Tnum) return INTERPRET_ERROR;

                // samotna operace a nastaveni typu vysledku
                (((tTableItem)(I->addr1))->value.Vnum) = (((tTableItem)(I->addr2))->value.Vnum) - (((tTableItem)(I->addr3))->value.Vnum);
                ((tTableItem)(I->addr1))->type = Tnum;
            break;
        }

        case I_MUL:{
        /*****  (I_MUL, adr1, adr2, adr3)   *****/
        // adr1 - soucin
        // adr2, adr3 - nasobenec, nasobitel
        // adr1 = adr2 * adr3

                // osetreni moznych chybovych stavu interpretu
                if((((tTableItem)(I->addr2))->type) != Tnum) return INTERPRET_ERROR;
                if((((tTableItem)(I->addr3))->type) != Tnum) return INTERPRET_ERROR;

                // samotna operace a nastaveni typu vysledku
                (((tTableItem)(I->addr1))->value.Vnum) = (((tTableItem)(I->addr2))->value.Vnum) * (((tTableItem)(I->addr3))->value.Vnum);
                ((tTableItem)(I->addr1))->type = Tnum;
            break;
        }

        case I_DIV:{
        /*****  (I_DIV, adr1, adr2, adr3)   *****/
        // adr1 - podil
        // adr2, adr3 - delenec, delitel
        // adr1 = adr2 / adr3

                // osetreni moznych chybovych stavu interpretu
                if((((tTableItem)(I->addr2))->type) != Tnum) return INTERPRET_ERROR;
                if((((tTableItem)(I->addr3))->type) != Tnum) return INTERPRET_ERROR;

                // chyba pri deleni nulou
                if((((tTableItem)(I->addr3))->value.Vnum) == 0) return INTERPRET_ERROR;

                // samotna operace a nastaveni typu vysledku
                (((tTableItem)(I->addr1))->value.Vnum) = (((tTableItem)(I->addr2))->value.Vnum) / (((tTableItem)(I->addr3))->value.Vnum);
                ((tTableItem)(I->addr1))->type = Tnum;
            break;
        }

        case I_POWER:{
        /*****  (I_POWER, vysledek, cislo, mocnitel)   *****/

                // osetreni moznych chybovych stavu interpretu
                if((((tTableItem)(I->addr2))->type) != Tnum) return INTERPRET_ERROR;
                if((((tTableItem)(I->addr3))->type) != Tnum) return INTERPRET_ERROR;

                double output = 0;  //vysledek
                output = pow((((tTableItem)(I->addr2))->value.Vnum), (((tTableItem)(I->addr3))->value.Vnum));
                if(output == 0) return INTERPRET_ERROR; // osetreni zaporneho mocnitele
                else ((tTableItem)(I->addr1))->value.Vnum = output;
            break;
        }

//---------------------------- RELACNI OPERACE ---------------------------------//

        case I_EQUIV:{
        /*****  (I_EQUIV, adr1, adr2, adr3)   *****/
        // adr1 - vysledek: true/false - znaci splneni relacni operace
        // adr2, adr3 - hodnoty
        // napr. adr2 == adr3 ->vraci true/false v adr1

                // obe porovnavane hodnoty jsou typu number
                if(((((tTableItem)(I->addr2))->type) == Tnum) && ((((tTableItem)(I->addr3))->type) == Tnum)){
                    // porovnavane hodnoty jsou stejne -> vraci true
                    if((((tTableItem)(I->addr2))->value.Vnum) == (((tTableItem)(I->addr3))->value.Vnum)){
                        ((tTableItem)(I->addr1))->value.Vbool = true;
                    }
                    // porovnavane hodnoty jsou ruzne -> vraci false
                    else ((tTableItem)(I->addr1))->value.Vbool = false;
                }
                // obe porovnavane hodnoty jsou typu boolean
                else if(((((tTableItem)(I->addr2))->type) == Tbool) && ((((tTableItem)(I->addr3))->type) == Tbool)){
                    // porovnavane hodnoty jsou stejne -> vraci true
                    if((((tTableItem)(I->addr2))->value.Vbool) == (((tTableItem)(I->addr3))->value.Vbool)){
                        ((tTableItem)(I->addr1))->value.Vbool = true;
                    }
                    // porovnavane hodnoty jsou ruzne -> vraci false
                    else ((tTableItem)(I->addr1))->value.Vbool = false;
                }
                // obe porovnavane hodnoty jsou typu string
                else if(((((tTableItem)(I->addr2))->type) == Tstr) && ((((tTableItem)(I->addr3))->type) == Tstr)){
                ((tTableItem)(I->addr1))->value.Vbool = LexCompare(I_EQUIV, &(((tTableItem)(I->addr2))->value.Vstr), &(((tTableItem)(I->addr3))->value.Vstr));
                }

                else ((tTableItem)(I->addr1))->value.Vbool = false;
            break;
        }

        case I_GTR:{
        /*****  (I_GTR, adr1, adr2, adr3)   *****/
        // adr1 - vysledek: true/false - znaci splneni/nesplneni relacni operace
        // adr2, adr3 - hodnoty
        // napr. adr2 == adr3 ->vraci true/false v adr1

                // obe porovnavane hodnoty jsou typu number
                if(((((tTableItem)(I->addr2))->type) == Tnum) && ((((tTableItem)(I->addr3))->type) == Tnum)){
                    // porovnavane hodnoty jsou stejne -> vraci true
                    if((((tTableItem)(I->addr2))->value.Vnum) > (((tTableItem)(I->addr3))->value.Vnum)){
                        ((tTableItem)(I->addr1))->value.Vbool = true;
                    }
                    // porovnavane hodnoty jsou ruzne -> vraci false
                    else ((tTableItem)(I->addr1))->value.Vbool = false;
                }
                // obe porovnavane hodnoty jsou typu string
                else if(((((tTableItem)(I->addr2))->type) == Tstr) && ((((tTableItem)(I->addr3))->type) == Tstr)){
                ((tTableItem)(I->addr1))->value.Vbool = LexCompare(I_GTR, &(((tTableItem)(I->addr2))->value.Vstr), &(((tTableItem)(I->addr3))->value.Vstr));
                }

                else ((tTableItem)(I->addr1))->value.Vbool = false;
            break;
        }

        case I_GTR_EQL:{
        /*****  (I_GTR_EQL, adr1, adr2, adr3)   *****/
        // adr1 - vysledek: true/false - znaci splneni/nesplneni relacni operace
        // adr2, adr3 - hodnoty
        // napr. adr2 == adr3 ->vraci true/false v adr1

                // obe porovnavane hodnoty jsou typu number
                if(((((tTableItem)(I->addr2))->type) == Tnum) && ((((tTableItem)(I->addr3))->type) == Tnum)){
                    // porovnavane hodnoty jsou stejne -> vraci true
                    if((((tTableItem)(I->addr2))->value.Vnum) >= (((tTableItem)(I->addr3))->value.Vnum)){
                        ((tTableItem)(I->addr1))->value.Vbool = true;
                    }
                    // porovnavane hodnoty jsou ruzne -> vraci false
                    else ((tTableItem)(I->addr1))->value.Vbool = false;
                }
                // obe porovnavane hodnoty jsou typu string
                else if(((((tTableItem)(I->addr2))->type) == Tstr) && ((((tTableItem)(I->addr3))->type) == Tstr)){
                ((tTableItem)(I->addr1))->value.Vbool = LexCompare(I_GTR_EQL, &(((tTableItem)(I->addr2))->value.Vstr), &(((tTableItem)(I->addr3))->value.Vstr));
                }

                else ((tTableItem)(I->addr1))->value.Vbool = false;
            break;
        }

        case I_LESS:{
        /*****  (I_LESS, adr1, adr2, adr3)   *****/
        // adr1 - vysledek: true/false - znaci splneni/nesplneni relacni operace
        // adr2, adr3 - hodnoty
        // napr. adr2 == adr3 ->vraci true/false v adr1

                // obe porovnavane hodnoty jsou typu number
                if(((((tTableItem)(I->addr2))->type) == Tnum) && ((((tTableItem)(I->addr3))->type) == Tnum)){
                    // porovnavane hodnoty jsou stejne -> vraci true
                    if((((tTableItem)(I->addr2))->value.Vnum) < (((tTableItem)(I->addr3))->value.Vnum)){
                        ((tTableItem)(I->addr1))->value.Vbool = true;
                    }
                    // porovnavane hodnoty jsou ruzne -> vraci false
                    else ((tTableItem)(I->addr1))->value.Vbool = false;
                }
                // obe porovnavane hodnoty jsou typu string
                else if(((((tTableItem)(I->addr2))->type) == Tstr) && ((((tTableItem)(I->addr3))->type) == Tstr)){
                ((tTableItem)(I->addr1))->value.Vbool = LexCompare(I_LESS, &(((tTableItem)(I->addr2))->value.Vstr), &(((tTableItem)(I->addr3))->value.Vstr));
                }

                else ((tTableItem)(I->addr1))->value.Vbool = false;
            break;
        }

        case I_LESS_EQL:{
        /*****  (I_LESS_EQL, adr1, adr2, adr3)   *****/
        // adr1 - vysledek: true/false - znaci splneni/nesplneni relacni operace
        // adr2, adr3 - hodnoty
        // napr. adr2 == adr3 ->vraci true/false v adr1

                // obe porovnavane hodnoty jsou typu number
                if(((((tTableItem)(I->addr2))->type) == Tnum) && ((((tTableItem)(I->addr3))->type) == Tnum)){
                    // porovnavane hodnoty jsou stejne -> vraci true
                    if((((tTableItem)(I->addr2))->value.Vnum) <= (((tTableItem)(I->addr3))->value.Vnum)){
                        ((tTableItem)(I->addr1))->value.Vbool = true;
                    }
                    // porovnavane hodnoty jsou ruzne -> vraci false
                    else ((tTableItem)(I->addr1))->value.Vbool = false;
                }
                // obe porovnavane hodnoty jsou typu string
                else if(((((tTableItem)(I->addr2))->type) == Tstr) && ((((tTableItem)(I->addr3))->type) == Tstr)){
                ((tTableItem)(I->addr1))->value.Vbool = LexCompare(I_LESS_EQL, &(((tTableItem)(I->addr2))->value.Vstr), &(((tTableItem)(I->addr3))->value.Vstr));
                }

                else ((tTableItem)(I->addr1))->value.Vbool = false;
            break;
        }

        case I_NON_EQUIV:{
                /*****  (I_NON_EQUIV, adr1, adr2, adr3)   *****/
                // adr1 - vysledek: true/false - znaci splneni relacni operace
                // adr2, adr3 - hodnoty
                // napr. adr2 == adr3 ->vraci true/false v adr1

                // obe porovnavane hodnoty jsou typu number
                if(((((tTableItem)(I->addr2))->type) == Tnum) && ((((tTableItem)(I->addr3))->type) == Tnum)){
                    // porovnavane hodnoty jsou stejne -> vraci true
                    if((((tTableItem)(I->addr2))->value.Vnum) != (((tTableItem)(I->addr3))->value.Vnum)){
                        ((tTableItem)(I->addr1))->value.Vbool = true;
                    }
                    // porovnavane hodnoty jsou ruzne -> vraci false
                    else ((tTableItem)(I->addr1))->value.Vbool = false;
                }
                // obe porovnavane hodnoty jsou typu boolean
                else if(((((tTableItem)(I->addr2))->type) == Tbool) && ((((tTableItem)(I->addr3))->type) == Tbool)){
                    // porovnavane hodnoty jsou stejne -> vraci true
                    if((((tTableItem)(I->addr2))->value.Vbool) != (((tTableItem)(I->addr3))->value.Vbool)){
                        ((tTableItem)(I->addr1))->value.Vbool = true;
                    }
                    // porovnavane hodnoty jsou ruzne -> vraci false
                    else ((tTableItem)(I->addr1))->value.Vbool = false;
                }
                // obe porovnavane hodnoty jsou typu string
                else if(((((tTableItem)(I->addr2))->type) == Tstr) && ((((tTableItem)(I->addr3))->type) == Tstr)){
                ((tTableItem)(I->addr1))->value.Vbool = LexCompare(I_NON_EQUIV, &(((tTableItem)(I->addr2))->value.Vstr), &(((tTableItem)(I->addr3))->value.Vstr));
                }

                else ((tTableItem)(I->addr1))->value.Vbool = false;
            break;
        }
//---------------------------- OPERACE NAD RETEZCI ---------------------------------//

        case I_CONCAT:{
        /*  (I_CONCAT, adr1, adr2, adr3)
            adr1 - vysledek
            adr2, adr3 - spojovane stringy
            realizace: strcat(dest, src)
                       dest - misto, kam se ulozi vysledek
                       dest, src - spojovane stringy
            Moznosti:
                       1) (I_CONCAT, a, a, b) -> napr.: a = a..b // prepis adr1
                       2) (I_CONCAT, b, a, b) -> napr.: b = a..b // prepis adr3
                       3) (I_CONCAT, c, a, b) -> napr.: c = a..b
        */

                // osetreni moznych chybovych stavu interpretu
                if((((tTableItem)(I->addr2))->type) != Tstr) return INTERPRET_ERROR;
                if((((tTableItem)(I->addr3))->type) != Tstr) return INTERPRET_ERROR;

                string input;   // pomocna promenna pro uchovani puvodni hodnoty addr2
                strInit(&input);

                copyString(&input, &(((tTableItem)I->addr2)->value.Vstr));  // uchovani puvodni hodnoty addr2
                //strcat(((tTableItem)(I->addr2))->value.Vstr.str, ((tTableItem)(I->addr3))->value.Vstr.str);    // konkatenace: addr2 = addr2..addr3
                int adr3len=strlen(((tTableItem)(I->addr3))->value.Vstr.str);
                for(int i=0;i<adr3len;i++){
                    strAddChar(&(((tTableItem)I->addr2)->value.Vstr),(((tTableItem)I->addr3)->value.Vstr.str[i]));
                }
                // pokud na adr1 neni stejna promenna jako na adr2 - zkopirujeme vysledek konkatenace z adr2 do adr1
                // a nastavime puvodni hodnotu do adr2
                if(strcmp(((tTableItem)(I->addr1))->name.str, ((tTableItem)(I->addr2))->name.str) != 0){ // addr1 != addr2
                    if(((tTableItem)I->addr1)->type==Tundef){
                        //printf("adr1 je undef\n");
                        strInit(&(((tTableItem)I->addr1)->value.Vstr));
                    }
                    copyString(&(((tTableItem)I->addr1)->value.Vstr), &(((tTableItem)I->addr2)->value.Vstr));   // ulozeni vysledku konkatenace do addr1
                    ((tTableItem)(I->addr1))->type = Tstr;  // nastaveni typu vysledku

                    copyString(&(((tTableItem)I->addr2)->value.Vstr), &input);  // nastaveni puvodni hodnoty addr2
                }
                strFree(&input);
            break;
        }

        case I_FIND:{
        /*****  (I_FIND, vysledek, retezec, hledany podretezec)   *****/
        // vyysledek = true/false podle toho, jestli zadany podretezec byl v retezci nalezen/nenalezen

                // pokud je typ adr2 nebo adr3 ruzny od Tstr, tak je vracen nil
                if(((((tTableItem)(I->addr2))->type) != Tstr) || ((((tTableItem)(I->addr3))->type) != Tstr))
                    ((tTableItem)(I->addr1))->type = Tundef;

                // jinak je vracen vysledek hledani
                else {
                    int resultFind = 0; //
                    resultFind = find(&(((tTableItem)I->addr2)->value.Vstr), &(((tTableItem)I->addr3)->value.Vstr));
                     // string nebyl nalezen, vracime false
                    if(resultFind == -1){
                        ((tTableItem)(I->addr1))->type = Tbool;
                        ((tTableItem)I->addr1)->value.Vbool = false;
                    }
                    // string byl nalezen, vracime cislo kde
                    else{
                        ((tTableItem)(I->addr1))->type = Tnum;
                        ((tTableItem)I->addr1)->value.Vnum = resultFind;
                    }
                }
            break;
        }

        case I_SORT:{
        /*****  (I_SORT, adr1, adr2, NULL)   *****/
        // adr1 - vysledny serazeny retezec
        // adr2 - razeny retezec

                // adr2 je jineho datoveho typu nez string - je vracen nil
                if((((tTableItem)(I->addr2))->type) != Tstr) ((tTableItem)(I->addr1))->type = Tundef;
                // jinak je vracen serazeny retezec
                else {
                    string input;   // pomocna promenna pro uchovani puvodni hodnoty addr2
                    input = sort(&(((tTableItem)I->addr2)->value.Vstr));
                    if(((tTableItem)I->addr1)->type==Tundef){
                        strInit(&(((tTableItem)I->addr1)->value.Vstr));
                    }
                    copyString(&(((tTableItem)I->addr1)->value.Vstr), &input);
                    ((tTableItem)I->addr1)->type=Tstr;
                    strFree(&input);
                }
            break;
        }

        case I_SUBSTR:{
        /*****  (I_SUBSTR, vysledek, NULL, NULL)   *****/
        /*****  (I_SUBSTR, retezec, zacatek podretezce, konec podretezce)   *****/
        // (I_SUBSTR, retezec, new_par1, new_par2) //
        
                // pokud je adr2 a adr3 rovna NULL, tak adr1 obsahuje adresu, na kterou bude ulozen vysledek -> ulozime si ji do pomocne promenne
                if((((tTableItem)(I->addr2)) == NULL) && (((tTableItem)(I->addr3)) == NULL)){
                        output_ptr = ((tTableItem)(I->addr1));
                        substr_cont = true;
                }
                else{
                    // pokud prisla instrukce (I_SUBSTR, vysledek, NULL, NULL), tak muzeme pokracovat ve vykonani instrukce I_SUBSTR
                    if(substr_cont == true){
                            // osetreni typu 1.parametru - retezec
                            if(((tTableItem)(I->addr1))->type != Tstr){ // pokud retezec neni typu string - vraci NULL
                                output_ptr->type = Tundef;
                            }
                            // osetreni vstupni hodnoty 1.parametru - neprazdny retezec
                            else if(((tTableItem)(I->addr1))->value.Vstr.str == NULL){ // pokud retezec neni typu string - vraci NULL
                                output_ptr->type = Tundef;
                            }
                            // osetreni typu 2.parametru - number
                            else if(((tTableItem)(I->addr2))->type != Tnum){ // pokud retezec neni typu string - vraci NULL
                                output_ptr->type = Tundef;
                            }
                            // osetreni typu 3.parametru - number
                            else if(((tTableItem)(I->addr3))->type != Tnum){ // pokud retezec neni typu string - vraci NULL
                                output_ptr->type = Tundef;
                            }
                            else{
                                output_ptr->value.Vstr = substr(&(((tTableItem)I->addr1)->value.Vstr), ((tTableItem)(I->addr2))->value.Vnum, ((tTableItem)(I->addr3))->value.Vnum);
                                output_ptr->type = Tstr;
                            }
                    }
                    else return INTERPRET_ERROR;
                }
            break;
        }

//-------------------- INSTRUKCE PRO NACTENI ZE VSTUPU --------------------//

        case I_READ_NUM:{
        /*****  (I_READ_NUM, vysledek, NULL, NULL)   *****/
        // instrukce nacte ciselnou hodnotu ze vstupu
                if(((tTableItem)I->addr1)->type==Tstr){
                    printf("nacitam cislo do stringu\n");
                    strFree(&(((tTableItem)I->addr1)->value.Vstr));
                }
                ((tTableItem)I->addr1)->type=Tnum;
                scanf("%lf", &(((tTableItem)I->addr1)->value.Vnum));
            break;
        }

        case I_READ_STR_EOL:{
        /*****  (I_READ_STR_EOL, vysledek, NULL, NULL)   *****/
        // instrukce nacte string ze vstupu po konec radku

                int c = 0;
                string input;
                strInit(&input);

                while((c = getchar()) != '\n'){
                    strAddChar(&input,c);
                }
                ((tTableItem)I->addr1)->type=Tstr;
                copyString(&(((tTableItem)I->addr1)->value.Vstr),&input);
                strFree(&input);
            break;
        }

        case I_READ_STR_EOF:{
        /*****  (I_READ_STR_EOF, vysledek, NULL, NULL)   *****/
        // instrukce nacte string ze vstupu po EOF

                int c = 0;
                string input;
                strInit(&input);

                while((c = getchar()) != EOF){
                    strAddChar(&input,c);
                }
                ((tTableItem)I->addr1)->type=Tstr;
                copyString(&(((tTableItem)I->addr1)->value.Vstr),&input);
                strFree(&input);
            break;
        }

        case I_READ_STR_CNT:{
        /*****  (I_READ_STR_CNT, vysledek, pocet_znaku, NULL)   *****/
        // instrukce nacte presny pocet znaku ze vstupu

                int c = 0, i = 0;
                int cnt = (((tTableItem)(I->addr2))->value).Vnum;

                string input;
                strInit(&input);

                while(i < cnt){
                    c = getchar();
                    strAddChar(&input,c);
                    i++;
                }
                ((tTableItem)I->addr1)->type=Tstr;
                copyString(&(((tTableItem)I->addr1)->value.Vstr),&input);
                strFree(&input);
            break;
        }

//-------------------------------------------------------------//

        case I_WRITE_NUM:{
        /*****  (I_WRITE_NUM, cislo, NULL, NULL)   *****/
        // instrukce pro vypis ciselne hodnoty na std. vystup

                printf("%g", (((tTableItem)(I->addr1))->value).Vnum);

            break;
        }

        case I_WRITE_STR:{
        /*****  (I_WRITE_STR, cislo, NULL, NULL)   *****/
        // instrukce pro vypis retezce na vystup

                printf("%s", ((tTableItem)I->addr1)->value.Vstr.str);
            break;
        }

        case I_WRITE_VAR:{
        /*****  (I_WRITE_VAR, promenna, NULL, NULL)   *****/
        // instrukce vypise hodnotu promenne na vystup

                // promenna je typu number
                if((((tTableItem)(I->addr1))->type) == Tnum){
                     printf("%g", (((tTableItem)(I->addr1))->value).Vnum);

                }
                // promenna je typu string
                else if((((tTableItem)(I->addr1))->type) == Tstr){
                    printf("%s", ((tTableItem)I->addr1)->value.Vstr.str);
                }
                // promenna je typu nil nebo bool
                else return INTERPRET_ERROR;
            break;
        }

//---------------------------- INSTRUKCE CYKLU IF ---------------------------------//

        case I_IF:{
        /*****  (I_IF, adr1, adr2, NULL)   *****/
        /*
        adr1 - vysledek podminky: true/false/nil
        adr2 - hodnota zanoreni

        - osetreni podminky:
        podminka neni splnena - preskakuje instrukce az najdeme I_ELSE
        podminka je splnena - pokracujeme nasledujici instrukci
        */

                // adr1 je typu number - vzdy true
                if((((tTableItem)(I->addr1))->type) == Tnum){
                     pushStackCond(&SCond, true, NULL); // uchovani vyhodnoceni podminky
                     pushStackCond(&SRank, (int)I->addr2, NULL);    // uchovani hodnoty zanoreni
                }
                // adr1 je typu string - vzdy true
                else if((((tTableItem)(I->addr1))->type) == Tstr){
                    pushStackCond(&SCond, true, NULL);  // uchovani vyhodnoceni podminky
                    pushStackCond(&SRank, (int)I->addr2, NULL); // uchovani hodnoty zanoreni
                }
                // adr1 je typu boolean - true/false
                else if((((tTableItem)(I->addr1))->type) == Tbool){
                    // podminka neni splnena - hledame instrukci I_IF_ELSE
                    if(((tTableItem)I->addr1)->value.Vbool == false){
                        pushStackCond(&SCond, false, NULL); // uchovani vyhodnoceni podminky
                        pushStackCond(&SRank, (int)I->addr2, NULL); // uchovani hodnoty zanoreni

                        int topRank = topStackCond(&SRank); // ulozeni hodnoty zanoreni z vrcholu zasobniku do pomocne promenne

                         while(1){
                            while((I->instType) != I_IF_END){
                                    Succ(instrList);
                                    I = ListCopyActive(instrList);
                            }
                            // byla nalezena instrukce I_IF_END - testujeme, zda je jeji hodnota zanoreni stejna jako hodnota z vrcholu zasobniku hodnot zanoreni
                            // hodnota zanoreni je stejna - posuneme se na dalsi instrukci
                            if((int)I->addr2 == topRank ){
                                break;
                            }
                            // hodnota zanoreni neni stejna, pokracujeme v hledani
                            else{
                                Succ(instrList);
                                I = ListCopyActive(instrList);
                            }
                         }

                    }
                    // podminka je splnena - pokracujeme nasledujici instrukci
                    else{
                        pushStackCond(&SCond, true, NULL);  // uchovani vyhodnoceni podminky
                        pushStackCond(&SRank, (int)I->addr2, NULL); // uchovani hodnoty zanoreni
                    }
                }
                // adr1 je typu Tundef - nil -> podminka neni splnena = false
                else{
                    pushStackCond(&SCond, false, NULL); // uchovani vyhodnoceni podminky
                    pushStackCond(&SRank, (int)I->addr2, NULL); // uchovani hodnoty zanoreni

                    int topRank = topStackCond(&SRank); // ulozeni hodnoty zanoreni z vrcholu zasobniku do pomocne promenne

                     while(1){
                        while((I->instType) != I_IF_END){
                                Succ(instrList);
                                I = ListCopyActive(instrList);
                        }
                        // byla nalezena instrukce I_IF_END - testujeme, zda je jeji hodnota zanoreni stejna jako hodnota z vrcholu zasobniku hodnot zanoreni
                        // hodnota zanoreni je stejna - posuneme se na dalsi instrukci
                        if((int)I->addr2 == topRank ){
                            break;
                        }
                        // hodnota zanoreni neni stejna, pokracujeme v hledani
                        else{
                            Succ(instrList);
                            I = ListCopyActive(instrList);
                        }
                     }
                }
            break;
        }

        case I_IF_END:
        /*****  (I_IF_END, NULL, adr2, NULL)   *****/
        // konec tela podminky IF
                break;

        case I_ELSE:{
        /*****  (I_ELSE, NULL, adr2, NULL)   *****/
        // adr2 - hodnota zanoreni
        // zacatek vetve else - testujeme, zda byl/nebyl vykonan odpovidajici if
        // pokud ano, tak hledame odpovidajici instrukci I_END
        // pokud ne, tak pokracujeme nasledujici instrukci

                bool done_if = topStackCond(&SCond);    // ulozeni vysledku podminky z vrcholu zasobniku do pomocne promenne
                popStackCond(&SCond);

                // odpovidajici instrukce IF jiz byla vykonana - hledame odpovidajici instrukci I_END
                if(done_if == true){
                    int topRank = topStackCond(&SRank);

                     while(1){
                        while((I->instType) != I_END){
                                Succ(instrList);
                                I = ListCopyActive(instrList);
                        }
                        // byla nalezena instrukce I_END - testujeme, zda je jeji hodnota zanoreni stejna jako hodnota z vrcholu zasobniku hodnot zanoreni
                        // hodnota zanoreni je stejna - posuneme se na dalsi instrukci a odstranim vrchol ze zasobniku hodnot zanoreni
                        if((int)I->addr2 == topRank ){
                            popStackCond(&SRank);
                            break;
                        }
                        // hodnota zanoreni neni stejna, pokracujeme v hledani
                        else{
                            Succ(instrList);
                            I = ListCopyActive(instrList);
                        }
                     }
                }
            break;
        }

        case I_END:{
        /*****  (I_END, NULL, NULL, NULL)   *****/
        // konec cyklu if - else

                //v pripade, ze se vykona else, nutno pop cisla ze stacku -> jdeme o uroven nize
                popStackCond(&SRank);

            break;
        }

//---------------------------- INSTRUKCE CYKLU WHILE ---------------------------------//

        case I_WHILE_START:{
        /*****  (I_WHILE_START, NULL, adr2, NULL)   *****/
        // adr2 - hodnota zanoreni

                pushStackCond(&SAddr,0,instrList->Active);  // vlozeni adresy aktivni instrukce do zasobniku
                pushStackCond(&SRankWhile, (int)I->addr2, NULL);   // vlozeni hodnoty zanoreni do zasobniku
            break;
        }

        case I_WHILE_COND:{
        /*****  (I_WHILE_COND, adr1, adr2, NULL)   *****/
        // adr1 - vysledek podminky: true/false/nil
        // adr2 - hodnota tanoreni

            // adr1 je typu number
                if((((tTableItem)(I->addr1))->type) == Tnum){
                     pushStackCond(&SCondWhile, true, NULL);    // vlozeni hodnoty true/false do zasobniku dle splnene podminky while
                }
                // adr1 je typu string
                else if((((tTableItem)(I->addr1))->type) == Tstr){
                     pushStackCond(&SCondWhile, true, NULL);    // vlozeni hodnoty true/false do zasobniku dle splnene podminky while
                }
                // adr1 je typ boolean
                else if((((tTableItem)(I->addr1))->type) == Tbool){
                    // podminka while neni splnena
                    if(((tTableItem)I->addr1)->value.Vbool == false){
                        pushStackCond(&SCondWhile, false, NULL);    // vlozeni hodnoty true/false do zasobniku dle splnene podminky while

                        int topRankWhile = topStackCond(&SRankWhile);   // ulozeni hodnoty zanoreni do pomocne promenne

                        while(1){
                             while((I->instType) != I_WHILE_END){
                                Succ(instrList);
                                I = ListCopyActive(instrList);
                             }
                             // byla nalezena instrukce I_WHILE_END - testujeme, zda je jeji hodnota zanoreni stejna jako hodnota z vrcholu zasobniku hodnot zanoreni
                             // hodnota zanoreni je stejna - posuneme se na dalsi instrukci
                             if((int)I->addr2 == topRankWhile ){
                                popStackCond(&SCondWhile);                  // odstranime zaznam o provedeni whilu
                                popStackCond(&SAddr);                       // odstranime adresu pro skok na zacatek whilu
                                popStackCond(&SRankWhile);                  // odstranime hodnotu zanoreni
                                break;
                             }
                             // hodnota zanoreni neni stejna - pokracujeme v hledani
                             else{
                                Succ(instrList);
                                I = ListCopyActive(instrList);
                             }
                         }
                    }
                    // podminka whilu je splnena
                    else{
                     pushStackCond(&SCondWhile, true, NULL);    // vlozeni hodnoty true/false do zasobniku dle splnene podminky while
                    }
                }
                // typ Tundef - resi NIL - podminka if neni pravdiva
                else{
                     pushStackCond(&SCondWhile, false, NULL);    // vlozeni hodnoty true/false do zasobniku dle splnene podminky while
                     int topRankWhile = topStackCond(&SRankWhile);

                     while(1){
                             while((I->instType) != I_WHILE_END){
                                Succ(instrList);
                                I = ListCopyActive(instrList);
                             }
                             // byla nalezena instrukce I_WHILE_END - testujeme, zda je jeji hodnota zanoreni stejna jako hodnota z vrcholu zasobniku hodnot zanoreni
                             // hodnota zanoreni je stejna - posuneme se na dalsi instrukci
                             if((int)I->addr2 == topRankWhile ){
                                popStackCond(&SCondWhile);                  // odstranime zaznam o provedeni whilu
                                popStackCond(&SAddr);                  // odstranime adresu pro skok na zacatek whilu
                                popStackCond(&SRankWhile);                  // odstranime zaznam o zanoreni
                                break;
                             }
                             // hodnota zanoreni neni stejna - pokracujeme v hledani
                             else{
                                Succ(instrList);
                                I = ListCopyActive(instrList);
                             }
                         }
                }
            break;
        }

        case I_WHILE_END:{
        /*****  (I_WHILE_END, NULL, adr2, NULL)   *****/

                if((int)I->addr2 == topStackCond(&SRankWhile)){    // narazili jsme na spravny I_WHILE_END
                    // while byl proveden - nastavime aktivitu na adresu z vrcholu zasobniku
                    if(topStackCond(&SCondWhile) == true){          // while se provedl, budeme se vracet na zacatek
                        instrList->Active = topStackAddr(&SAddr);   // nastavime adresu na zacatek whilu
                        popStackCond(&SCondWhile);                  // odstranime zaznam o provedeni whilu
                    }
                    // while nebyl proveden - pokracujeme nasledujici instrukci
                    else{
                        popStackCond(&SCondWhile);                  // odstranime zaznam o provedeni whilu
                        popStackCond(&SAddr);                       // odstranime adresu pro skok na zacatek whilu
                        popStackCond(&SRankWhile);                  // odstranime zaznam o zanoreni
                    }
                }
            break;
        }


//-------------------------------------------------------------//

        case I_FUNC_JMP:{
        /*****  (I_FUNC_JMP, adresa, NULL, NULL)   *****/
        // skoci na zadanou adresu

            pushStackCond(&SAddrAct,0,instrList->Active);  // vlozeni adresy aktivni instrukce do zasobniku
            instrList->Active = (tListItem *)(I->addr1);
            break;
        }

        case I_FUNC_START:{
        /*****  (I_FUNC_START, NULL, NULL, NULL)   *****/


            break;
        }

        case I_FUNC_END:{
        /*****  (I_FUNC_END, NULL, NULL, NULL)   *****/
        // nastaveni aktivni adresy - kde jsme skoncili v mainu

                //instrList->Active = save_active;
                if(DEBUG)printf("\ninsttype%d\n", instrList->Active->Instruction.instType);
                if(DEBUG)printf("konec i_func_end\n");

                instrList->Active = topStackAddr(&SAddrAct);   // nastavime aktivitu na adresu z vrcholu zasobniku
                popStackCond(&SAddrAct);

            break;
        }

//-------------------------------------------------------------//


    }
    if(DEBUG)PrintHT(ST);

    Succ(instrList);
  }

  // korektni uvolneni pouzivanych stacku
  freeStackCond(&SCond);
  freeStackCond(&SRank);
  freeStackCond(&SAddr);
  freeStackCond(&SCondWhile);
  freeStackCond(&SRankWhile);
  freeStackCond(&SAddrAct);

}
