/*
###############################################################################
###############################################################################
########                                                               ########
########    Implementace interpretu imperativn�ho jazyka IFJ11         ########
######## ------------------------------------------------------------- ########
########    http://goo.gl/368JS                                        ########
######## ------------------------------------------------------------- ########
########   Vypracovali: Pavl�na Puncocharova  <xpunco01@fit.vutbr.cz>  ########
########                Radek Sedlak          <xsedla90@fit.vutbr.cz>  ########
########                Michal S�eptun         <xseptu00@fit.vutbr.cz>  ########
########                Martin Soulak         <xsoula02@fit.vutbr.cz>  ########
########                Jakub Vones�           <xvones02@fit.vutbr.cz>  ########
########                                                               ########
###############################################################################
###############################################################################
*/

#include "parser.h"




tSymbolTable *table;    // globalni promenna uchovavajici tabulku symbolu
tListOfInstr *list;     // globalni promenna uchovavajici seznam instrukci
int token;              // globalni promenna, ve ktere bude ulozen aktualni token
string attr;            // globalni promenna, ve ktere bude ulozen atribut tokenu
string act_func_name;   // globalni promenna, ve ktere bude ulozen nazev aktualne zpracovavane funkce
int counterVar = 1;
unsigned int if_counter=1;
unsigned int while_counter=1;
SetList function_list;

void generateVariable(string *var,int unique){

  if(unique){
        if(DEBUG)printf("generuji unikat variable\n");
      strClear(var);
      for(int i=0;  i<act_func_name.length;  i++){
        if(act_func_name.str[i]!='\0')
            strAddChar(var,act_func_name.str[i]);
      }
      strAddChar(var, '$');
      strAddChar(var, '$');
      int i;
      i = counterVar;
      while (i != 0){
        strAddChar(var, (char)(i % 10 + '0'));
        i = i / 10;
      }
      counterVar ++;
  }
  else{
    string tmp;
    strInit(&tmp);
    if(DEBUG)printf("string s delkou %d, a hodnotou %s\n",var->length,var->str);
    copyString(&tmp,var);
    strClear(var);
    if(DEBUG) printf("generuji promennou pro funkci\n");
    for(int i=0;  i<act_func_name.length;  i++){
        if(act_func_name.str[i]!='\0')
            strAddChar(var,act_func_name.str[i]);
    }
    strAddChar(var,'$');
    for(int i=0;i<tmp.length;i++){
        if(tmp.str[i]!='\0')
            strAddChar(var,tmp.str[i]);
    }
    if(DEBUG)printf("generuji variable > %s\n",var->str);
    strFree(&tmp);
  }
}

void generateInstruction(int instType, void *addr1, void *addr2, void *addr3){
// vlozi novou instrukci do seznamu instrukci
   tInstr I;
   I.instType = instType;
   I.addr1 = addr1;
   I.addr2 = addr2;
   I.addr3 = addr3;
   ListInsertLast(list, I);
}

int func_def_and_dec(){
    tTableItem f_name_adr;
    int result;
    switch(token){
        case ID:
            //<func> -> function id ( <f_params> ) <var_declaration> <command_sequence> end
            if(strcmp(attr.str,"main")==0){
                return OK;
            }
            else{
                //mam definici funkce
                // inicializace + alokace act_func_name - dealokovano
                strInit(&act_func_name);
                //strInit(&f_name);
                //zapamatuju si nazev funkce do glob promenne
                copyString(&act_func_name,&attr);
                generateVariable(&attr,1);
                insertnewHT(table,&attr,Tstr);
                f_name_adr=findHT(table,attr.str);
                strInit(&(f_name_adr->value.Vstr));
                copyString(&(f_name_adr->value.Vstr),&act_func_name);

                //vygenerovani instrukce pro zacatek funkce
                generateInstruction(I_FUNC_START,(void*)f_name_adr,NULL,NULL);
                //TODO > generace funkce do listu
                int result = InsertLast_TList(&function_list, &act_func_name, NULL,listGetPointerLast(list));
                // nacteni dalsiho tokenu
                token = getNextToken(&attr);
                if ( token <= LEXEM_BEGIN && token!=OK){
                    //strFree(&attr);
                    return token;
                }
                // zavolame funkci pro pravidlo func
                result=func();
                if(result!=OK)return result;
                //vygenerovani instrukce pro konec funkce
                generateInstruction(I_FUNC_END,NULL,NULL,NULL);
                // nacteni dalsiho tokenu
                token = getNextToken(&attr);
                if ( token <= LEXEM_BEGIN && token!=OK){
                    //strFree(&attr);
                    return token;
                }
                // dealokace act_func_name
                strFree(&act_func_name);
                //znovuzavolani sama sebe kvuli dalsi funkci
                result=func_def_and_dec();
                return result;
            }
        break;
        //pokud dostanu novou funkci
        case FUNCTION:
            //nacetl sem function
            //nactu dalsi token a volam sam sebe(rekurze)
            //<func_def_and_dec> -> <func> <func_def_and_dec>
            // nacteni dalsiho tokenu
            token = getNextToken(&attr);
            if ( token <= LEXEM_BEGIN && token!=OK){
                //strFree(&attr);
                return token;
            }
            result=func_def_and_dec();
            return result;
        break;

    }
    if(DEBUG)printf("vracim synt_ERR\n");
    return SYNTAX_ERROR;
}

int func(){
    int result;
    switch(token){
        case LEFT_BRACKET:{
            //po id funkce musi byt leva zatvorka
            // nacteni dalsiho tokenu
            token = getNextToken(&attr);
            if ( token <= LEXEM_BEGIN && token!=OK){
                return token;
            }
            result=f_params();
            if(result!=SYNTAX_OK)return result;
            if(token!=RIGHT_BRACKET) return SYNTAX_ERROR;
            // nacteni dalsiho tokenu
            token = getNextToken(&attr);
            if ( token <= LEXEM_BEGIN && token!=OK){
                return token;
            }
            result=var_declaration();
            if(result!=SYNTAX_OK)return result;
            result=command_sequence();
            if(result!=SYNTAX_OK) return result;
            if(token==END){
            return SYNTAX_OK;}
        }
    }
    return SYNTAX_ERROR;
}

int f_params(){
    tTableItem f_param_adr;
    int result;
    switch(token){
        case ID:
            //mam id parametru
            //ulozim do tabulky symbolu
            generateVariable(&attr,NOT_UNIQUE);
            insertnewHT(table,&attr,Tundef);
            f_param_adr=findHT(table,attr.str);
            InsertLast_ParamItem(function_list.last,(void *)f_param_adr);
            // nacteni dalsiho tokenu
            token = getNextToken(&attr);
            if ( token <= LEXEM_BEGIN && token!=OK){
                return token;
            }
            if(token==RIGHT_BRACKET)return SYNTAX_OK;
            if(token!=COMMA) return SYNTAX_ERROR;
            // nacteni dalsiho tokenu
            token = getNextToken(&attr);
            if ( token <= LEXEM_BEGIN && token!=OK){
                return token;
            }
            result=f_params();
            return result;
        break;
        case RIGHT_BRACKET:
            //pro epsilon pravidlo
            return SYNTAX_OK;
        break;
    }
  return SYNTAX_ERROR;
}

int main_body(){
    int result;
    if(DEBUG)printf("\nnacitam main\n");
    // z func_def_and_dec musel dojit token ID(main) jinak by to sem nedoslo a hodilo by to synt | sem error
    switch(token){
        case ID:
            strInit(&act_func_name);
            copyString(&act_func_name,&attr);
            // nacteni dalsiho tokenu
            token = getNextToken(&attr);
            if ( token <= LEXEM_BEGIN && token!=OK){
                return token;
            }
            //nacteni zavorky (
            if(token==LEFT_BRACKET){
                // nacteni dalsiho tokenu
                token = getNextToken(&attr);
                if ( token <= LEXEM_BEGIN && token!=OK){
                    return token;
                }
                //nacteni zavorky )
                if(token==RIGHT_BRACKET){
                    generateInstruction(I_FUNC_START,NULL,NULL,NULL);//vygeneruju instrukci zacatku funkce main (vsechny adr=NULL)
                    list->Active=list->Last;
                    // nacteni dalsiho tokenu
                    token = getNextToken(&attr);
                    if ( token <= LEXEM_BEGIN && token!=OK){
                        return token;
                    }
                    result=var_declaration();
                    if(result!=SYNTAX_OK)return result;
                    result=command_sequence();
                    if(result!=SYNTAX_OK)return result;
                    if(token==END){
                        // nacteni dalsiho tokenu
                        token = getNextToken(&attr);
                        if ( token <= LEXEM_BEGIN && token!=OK){
                            return token;
                        }
                        if(token == SEMICOLON){
                            // nacteni dalsiho tokenu
                            token = getNextToken(&attr);
                            if ( token <= LEXEM_BEGIN && token!=OK){
                                return token;
                            }
                            if(token==END_OF_FILE){
                                 strFree(&act_func_name);
                                 return SYNTAX_OK;
                            }
                        }

                    }
                }
            }
        break;
    }
    return SYNTAX_ERROR;
}

int var_declaration(){
    tTableItem adr1;
    tTableItem adr2;
    int result;
    switch(token){
        case LOCAL:
            // nacteni dalsiho tokenu
            token = getNextToken(&attr);
            if ( token <= LEXEM_BEGIN && token!=OK){
                return token;
            }
            if(token!=ID) return SYNTAX_ERROR;
            //ulozit id do tab symbolu
            generateVariable(&attr,0);
            result=insertnewHT(table,&attr,Tundef);

            if(result!=SYNTAX_OK)return SEM_ERROR;
            adr1=findHT(table,attr.str);
            // nacteni dalsiho tokenu
            token = getNextToken(&attr);
            if ( token <= LEXEM_BEGIN && token!=OK){
                return token;
            }
            if(token==SEMICOLON){
                // nacteni dalsiho tokenu
                token = getNextToken(&attr);
                if ( token <= LEXEM_BEGIN && token!=OK){
                    return token;
                }
                return var_declaration();
            }
            if(token!=EQL) return SYNTAX_ERROR;
            // nacteni dalsiho tokenu
            token = getNextToken(&attr);
            if ( token <= LEXEM_BEGIN && token!=OK){
                return token;
            }
            result=exprSA(&adr2);
            if(result!=OK){
                if(DEBUG) printf("exprSA error\n");
                return result;
            }if(DEBUG) printf("\n\ntyp je:%d\n\n",adr2->type);
            //nageneruju instrukci pro setvalue na adr1 z adr2
            switch(adr2->type){
                    case Tnum:
                        generateInstruction(I_SETVALUE_NUM,(void *)adr1,(void *)adr2,NULL);
                    break;
                    case Tstr:
                        generateInstruction(I_SETVALUE_STR,(void *)adr1,(void *)adr2,NULL);
                    break;
                    case Tbool:
                        generateInstruction(I_SETVALUE_BOOL,(void *)adr1,(void *)adr2,NULL);
                    break;
                    default:
                        generateInstruction(I_SETVAR,(void *)adr1,(void *)adr2,NULL);
                    break;
            }

            if(DEBUG) printf("vygeneroval sem instrukci\n mam token %d\n",token);
            if(token==SEMICOLON){
                // nacteni dalsiho tokenu
                token = getNextToken(&attr);
                if ( token <= LEXEM_BEGIN && token!=OK){
                    return token;
                }
                result=var_declaration();
                return result;
            }
        break;
        default:
            return SYNTAX_OK;
        break;
    }
    return SYNTAX_ERROR;
}

int command_sequence(){
    int result;
    switch(token){
        case END:
            return SYNTAX_OK;
        break;
        case ELSE:
            return OK;
        break;
        default:
            result=command();
            if(DEBUG)printf("mam token %d\n",token);
            if(result!=SYNTAX_OK)return result;
            if(token==END) return SYNTAX_OK;
            if(token==ELSE){if(DEBUG)printf("token je else\n"); return OK;}
            result=command_sequence();
            return result;
        break;
    }
    return SYNTAX_ERROR;
}
int command_sequence_n(){
    int result;
    switch(token){
        case ELSE:
            return SYNTAX_OK;
        break;
        default:
            result=command();
            if(result!=SYNTAX_OK)return result;
            if(token==END) return SYNTAX_OK;
            result=command_sequence();
            return result;
        break;
    }
    return SYNTAX_ERROR;
}
int command(){
    int result;
    tTableItem adr1;
    tTableItem adr2;
    TList *act_func;
    string PomAttr;
    switch(token){
        case ID:{
            if(DEBUG)printf("command\n mam id\n");
            TList *func=findTList(&function_list,&attr); //zjistim jestli nahodou nemam id funkce
            if(func!=NULL){//pokud sem ho nasel tak volam exprSA ktery ty fce zpracuje
                if(DEBUG) printf("je volana funkce bez prirazeni navratove hodnoty\n");
                result=exprSA(&adr1);//NULL - neni kam ukladat vysledek
                if(result!=OK){
                    if(DEBUG) printf("exprSA error\n");
                    return result;
                }
                if(token==SEMICOLON) {
                    token = getNextToken(&attr);
                    if ( token <= LEXEM_BEGIN && token!=OK){
                        return token;
                    }
                    if(DEBUG)printf("token je strednik\n");
                    return OK;
                }
                else return SYNTAX_ERROR;
            }
            //jinak mam id promenne
            generateVariable(&attr,0);
            adr1=findHT(table,attr.str);
            if(adr1==NULL) return SEM_ERROR;
            // nacteni dalsiho tokenu
            token = getNextToken(&attr);
            if ( token <= LEXEM_BEGIN && token!=OK){
                return token;
            }
            if(DEBUG)printf("ziskavam next token\n");
            if(token!=EQL) return SYNTAX_ERROR;
            if(DEBUG)printf("mam =\n");
            generateVariable(&attr,UNIQUE);
            result=insertnewHT(table,&attr,Tundef);
            if(result!=OK)return result;
            // nacteni dalsiho tokenu
            token = getNextToken(&attr);
            if ( token <= LEXEM_BEGIN && token!=OK){
                return token;
            }
            result=exprSA(&adr2);
            if(result!=OK){
                if(DEBUG) printf("exprSA error\n");
                return result;
            } if(DEBUG)printf("\n\ntyp je:%d\n\n",adr2->type);
            //nageneruju instrukci pro setvalue na adr1 z adr2
            switch(adr2->type){
                    case Tnum:
                        generateInstruction(I_SETVALUE_NUM,(void *)adr1,(void *)adr2,NULL);
                    break;
                    case Tstr:
                        generateInstruction(I_SETVALUE_STR,(void *)adr1,(void *)adr2,NULL);
                    break;
                    case Tbool:
                        generateInstruction(I_SETVALUE_BOOL,(void *)adr1,(void *)adr2,NULL);
                    break;
                    default:
                        generateInstruction(I_SETVAR,(void *)adr1,(void *)adr2,NULL);
                    break;
            }
            if(DEBUG) printf("nageneroval sem instrukci\n");
            if(token!=SEMICOLON) return SYNTAX_ERROR;
            token = getNextToken(&attr);
            if ( token <= LEXEM_BEGIN && token!=OK){
                return token;
            }
            return OK;
            break;
        }
        case WRITE:{
            if(DEBUG)printf("---command:write\n");
            // nacteni dalsiho tokenu
            token = getNextToken(&attr);
            if ( token <= LEXEM_BEGIN && token!=OK){
                return token;
            }
            result=write();
            if(DEBUG)printf("---command:write end result:%d\n",result);
            return result;

        }
        case IF:{
            int if_count=if_counter;
            if_counter++;
            if(DEBUG) printf("jsem v IF\n");
            // nacteni dalsiho tokenu
            token = getNextToken(&attr);
            if ( token <= LEXEM_BEGIN && token!=OK){
                return token;
            }
            result=exprSA(&adr1);
            generateInstruction(I_IF,(void *)adr1,(void *)if_count,NULL);
            if(result!=OK){
                if(DEBUG) printf("exprSA error\n");
                return result;
            }
            if(token!=THEN){
                if(DEBUG)printf("token !=then\n");
                return SYNTAX_ERROR;
            }
            if(DEBUG)printf("mam then\n");
            token = getNextToken(&attr);
            if ( token <= LEXEM_BEGIN && token!=OK){
                return token;
            }
            if(DEBUG) printf("volam command sequence v IF\n");
            result=command_sequence();
            if(result!=OK){ if(DEBUG)printf("result command seq v ifu !=OK\n");return result;}
            if(token!=ELSE){ if(DEBUG)printf("token!=ELSE");return SYNTAX_ERROR;}
            generateInstruction(I_IF_END,NULL,(void *)if_count,NULL);
            generateInstruction(I_ELSE,NULL,(void *)if_count,NULL);
            token = getNextToken(&attr);
            if ( token <= LEXEM_BEGIN && token!=OK){
                return token;
            }
            result=command_sequence();
            if(token!=END) return SYNTAX_ERROR;
            generateInstruction(I_END,NULL,(void *)if_count,NULL);
            if(DEBUG)printf("token je end\n");
            token = getNextToken(&attr);
            if ( token <= LEXEM_BEGIN && token!=OK){
                return token;
            }
            if(token==SEMICOLON) {
                token = getNextToken(&attr);
                if ( token <= LEXEM_BEGIN && token!=OK){
                    return token;
                }
                if(DEBUG)printf("token je strednik\n");
                return OK;
            }
        }
        case WHILE:
            if(DEBUG) printf("jsem ve WHILE\n");
            int while_count=while_counter;
            while_counter++;
            // nacteni dalsiho tokenu
            token = getNextToken(&attr);
            if ( token <= LEXEM_BEGIN && token!=OK){
                return token;
            }
            generateInstruction(I_WHILE_START,NULL,(void *)while_count,NULL);
            result=exprSA(&adr1);
            generateInstruction(I_WHILE_COND,(void *)adr1,(void *)while_count,NULL);
            if(result!=OK){
                if(DEBUG) printf("exprSA error\n");
                return result;
            }
            if(token!=DO){
                if(DEBUG)printf("token !=then\n");
                return SYNTAX_ERROR;
            }
            if(DEBUG)printf("mam then\n");
            token = getNextToken(&attr);
            if ( token <= LEXEM_BEGIN && token!=OK){
                return token;
            }
            if(DEBUG) printf("volam command sequence ve WHILE\n");
            result=command_sequence();
            if(result!=OK){ if(DEBUG)printf("result command seq ve while !=OK\n");return result;}
            if(token!=END) return SYNTAX_ERROR;
            generateInstruction(I_WHILE_END,NULL,(void *)while_count,NULL);
            if(DEBUG)printf("token je end\n");
            token = getNextToken(&attr);
            if ( token <= LEXEM_BEGIN && token!=OK){
                return token;
            }
            if(token==SEMICOLON) {
                token = getNextToken(&attr);
                if ( token <= LEXEM_BEGIN && token!=OK){
                    return token;
                }
                if(DEBUG)printf("token je strednik\n");
                return OK;
            }
        break;
        case RETURN:
            act_func=findTList(&function_list,&act_func_name);
            token = getNextToken(&attr);
            if ( token <= LEXEM_BEGIN && token!=OK){
                return token;
            }
                          strInit(&PomAttr);

            generateVariable(&PomAttr, UNIQUE);
              insertnewHT(table, &PomAttr, Tundef);
  act_func->table=findHT(table, PomAttr.str);



            result=exprSA(&act_func->table);
            if(result!=OK) return result;
                          generateInstruction(I_RETURN,(void *)act_func->table,NULL,NULL);
            token = getNextToken(&attr);
            if ( token <= LEXEM_BEGIN && token!=OK){;
                return token;
            }
            return OK;
        break;
    }
    return SYNTAX_ERROR;
}

int write(){
    int result;
    switch(token){
        case LEFT_BRACKET:
            if(DEBUG)printf("nacetl jsem levou zavorku od write\n");
            // nacteni dalsiho tokenu
            token = getNextToken(&attr);
            if ( token <= LEXEM_BEGIN && token!=OK){
                return token;
            }
            result=write_expr();
            if(result!=OK)return result;
            if(token!=SEMICOLON) return SYNTAX_ERROR;
            if(DEBUG)printf("nacetl jsem strednik od write\n");
            // nacteni dalsiho tokenu
            token = getNextToken(&attr);
            if ( token <= LEXEM_BEGIN && token!=OK){
                return token;
            }
            return SYNTAX_OK;
        break;
    }
    return SYNTAX_ERROR;


}

int write_expr(){
    if(DEBUG)printf("write_EXPR\n");
    int result;
    tTableItem adr1;
    double num;
    string tmp;
    switch(token){
        case RIGHT_BRACKET:
            //write je prazdny
            if(DEBUG)printf("nacetl sem pravou zavorku od write\n");
            token = getNextToken(&attr);
            if ( token <= LEXEM_BEGIN && token!=OK){
                return token;
            }
            return OK;
        break;
        case STR:
            strInit(&tmp);
            copyString(&tmp,&attr);
            generateVariable(&attr,1);
            if(DEBUG)printf("tisknu > %s\n",tmp.str);
            result = insertnewHT(table,&attr,Tstr);
            if(result!=OK)return result;
            adr1=findHT(table,attr.str);
            strInit(&(adr1->value.Vstr));
            copyString(&(adr1->value.Vstr),&tmp);
            strFree(&tmp);
            generateInstruction(I_WRITE_STR,(void *) adr1,NULL,NULL);
        break;
        case NUM:
            num=strtod(attr.str,NULL);
            generateVariable(&attr,1);
            result=insertnewHT(table,&attr,Tnum);
            if(result!=OK)return result;
            adr1=findHT(table,attr.str);
            adr1->value.Vnum=num;
            generateInstruction(I_WRITE_NUM,(void*) adr1,NULL,NULL);
        break;
        case ID:
            generateVariable(&attr,NOT_UNIQUE);
            adr1=findHT(table,attr.str);
            if(adr1==NULL) return SEM_ERROR;
            generateInstruction(I_WRITE_VAR,(void *)adr1,NULL,NULL);
        break;
        default:
            return SYNTAX_ERROR;
        break;
    }
    // nacteni dalsiho tokenu
    token = getNextToken(&attr);
    if ( token <= LEXEM_BEGIN && token!=OK){
        return token;
    }
    if(token==RIGHT_BRACKET){
        // nacteni dalsiho tokenu
        token = getNextToken(&attr);
        if ( token <= LEXEM_BEGIN && token!=OK){
            return token;
        }
        if(DEBUG)printf("write expr ok\n");
        return OK; //koncim write_expr
    }
    if(token!=COMMA) return SYNTAX_ERROR;
    //token je COMMA
    // nacteni dalsiho tokenu
    token = getNextToken(&attr);
    if ( token <= LEXEM_BEGIN && token!=OK){

        return token;
    }
    if(token==RIGHT_BRACKET) return SYNTAX_ERROR;
    result=write_expr();
    return result;
}


/***********************
 *
 * funkce pro pravidlo hlavni pravidlo
 * <program> ->  <func_def_and_dec> <main_body> <EOF>
 */

int program(){
  int result;
  if(token==FUNCTION){
        //function definition and declaration
        result = func_def_and_dec();
        if (result != SYNTAX_OK) return result;
        //main function
        result = main_body();
        if (result!= SYNTAX_OK) return result;
        // test, zda nasleduje konec souboru.
        if (token != END_OF_FILE) return SYNTAX_ERROR;
        // nagenerujeme instrukci konce programu
        generateInstruction(I_STOP, NULL, NULL, NULL);
        return SYNTAX_OK;
  }
  // pokud aktualni token je jiny nez function, jedna se o syntaktickou chybu
  return SYNTAX_ERROR;
}

/***************************************************
 * hlavni funkce parseru
 *
 ***************************************************/
int parse(tSymbolTable *ST, tListOfInstr *instrList){
    if(DEBUG){printf("*******************************************************\n");}
    if(DEBUG){printf("*          parser zacina pracovat                     *\n");}
    if(DEBUG){printf("*******************************************************\n");}
    int result;
    table = ST;
    list = instrList;
    strInit(&attr);
    //inicializace seznamu funkci
    setList(&function_list);
    //nactu token
    token = getNextToken(&attr);
    if ( token <= LEXEM_BEGIN && token!=OK){
     // nastala chyba jiz pri nacteni prvniho lexemu
    result = token;
  }
  else{
     result = program();
     if(DEBUG) printf("parser:program():END\n");
  }
  strFree(&attr);
  if(DEBUG){printf("*******************************************************\n");}
  if(DEBUG){printf("*          parser konci                               *\n");}
  if(DEBUG){printf("*******************************************************\n");}
  if(DEBUG)view_TList(&function_list);
  freeSetList(&function_list);
  return result;
}

void not_implemented(){
    printf("\nneni implementovano, koncim program\n");
    exit(EXIT_FAILURE);
}
void debug_exit(){
    printf("\ndebug: koncim\n");
    exit(EXIT_SUCCESS);
}
