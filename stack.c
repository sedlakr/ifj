/*
###############################################################################
###############################################################################
########                                                               ########
########    Implementace interpretu imperativn�ho jazyka IFJ11         ########
######## ------------------------------------------------------------- ########
########    http://goo.gl/368JS                                        ########
######## ------------------------------------------------------------- ########
########   Vypracovali: Pavl�na Puncocharova  <xpunco01@fit.vutbr.cz>  ########
########                Radek Sedlak          <xsedla90@fit.vutbr.cz>  ########
########                Michal S�eptun         <xseptu00@fit.vutbr.cz>  ########
########                Martin Soulak         <xsoula02@fit.vutbr.cz>  ########
########                Jakub Vones�           <xvones02@fit.vutbr.cz>  ########
########                                                               ########
###############################################################################
###############################################################################
*/
 
#include "stack.h"
#include "prec_table.h"

void initStack(TStack *i_stack){
     i_stack->first = NULL;
     i_stack->count = 0;
}

TStackItem *allocItem_Stack(){
     TStackItem *n_item = (TStackItem*)malloc(sizeof(TStackItem));
     if(n_item == NULL){
               return NULL;          
     }
     n_item->next  = NULL;
     return n_item;
}

void saveItem(TStackItem *n_item, int token, tTableItem table){
     n_item->token = token;
     n_item->table = table;
}

bool addItem_Stack(TStack *n_stack, int token, tTableItem table){
     if(n_stack == NULL){
          return false;
     }
     else{
          // alokovani + kontrola pameti
          TStackItem *n_item = allocItem_Stack(n_stack);        
          if(n_item == NULL) return false;
          
          // ulozeni dat
          saveItem(n_item, token, table);
          
          // svazani polozek
          TStackItem *novy = n_stack->first;
          n_stack->first   = n_item;
          n_stack->first->next = novy;
          ++(n_stack->count);
          
          return true;
     }
}

bool addStSRule_Stack(TStack *n_stack, int pos){
     if(n_stack == NULL){
          return false;
     }
     else if(n_stack->first == NULL){
          return false;
     }
     else{
          // alokovani + kontrola pameti
          TStackItem *n_item = allocItem_Stack(n_stack);        
          if(n_item == NULL) return false;
          
          // ulozeni dat
          saveItem(n_item, SI_SRule, NULL);
          if(pos==0){// svazani polozek - ulozeni na vrchol zasobniku
            n_item->next = n_stack->first;
            n_stack->first=n_item;
            ++(n_stack->count);
          }
          else{   // svazani polozek - ulozeni na druhe misto
            n_item->next = n_stack->first->next;
            n_stack->first->next = n_item;
            ++(n_stack->count);
          }
          
          return true;
     }
}

TStackItem *freeItem_Stack(TStack *stack, TStackItem *f_item){
     TStackItem *next = f_item->next;
     
     // + free na data
     free(f_item);
     --(stack->count);            
     return next;
}

void freeStack(TStack *stack){
     TStackItem *item = stack->first;
     while(item != NULL){
          item = freeItem_Stack(stack, item);
     }
     initStack(stack);
}

void freeFirst(TStack *stack){
     TStackItem *item;
     if(stack != NULL){
       if(stack->count > 0){       
          item = stack->first;
          stack->first = stack->first->next;
          --(stack->count);
          // + free na data
          free(item);       
       }           
     }
}

/*  Odstran� x polo�ek z vrcholu z�sobn�ku
 *  1 - z�sobn�k
 *  2 - po�et Item 
 */
void freeXItem(TStack *stack, int count){
      if(count > 0){
          int i;
          for(i = 0; i<count; i++){
              freeFirst(stack);
          }
      }
      return;
}

TStackItem *returnFirst_Item(TStack *stack){
     if(stack != NULL){
          return stack->first; 
     }
     else return NULL;      
}

void viewStack(TStack *stack){
     TStackItem *item = stack->first;
     while(item != NULL){
          printf("%d HTukaz:%d +++", item->token,(int) item->table);      
	  if (item->table!=NULL){
	    printf("tableTYP:%d ",item->table->type);
	    if(item->table->type==Tnum){
	      printf("tableValue NUM:%f\n", item->table->value.Vnum);
	    }	  
	  }
	  else{
	    printf("\n");
	  }
          item = item->next;
     }
     printf("\n\n"); 
}

