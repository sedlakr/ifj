/*
###############################################################################
###############################################################################
########                                                               ########
########    Implementace interpretu imperativn�ho jazyka IFJ11         ########
######## ------------------------------------------------------------- ########
########    http://goo.gl/368JS                                        ########
######## ------------------------------------------------------------- ########
########   Vypracovali: Pavl�na Pun�och��ov�  <xpunco01@fit.vutbr.cz>  ########
########                Radek Sedl�k          <xsedla90@fit.vutbr.cz>  ########
########                Michal �eptun         <xseptu00@fit.vutbr.cz>  ########
########                Martin �oul�k         <xsoula02@fit.vutbr.cz>  ########
########                Jakub Vone�           <xvones02@fit.vutbr.cz>  ########
########                                                               ########
###############################################################################
###############################################################################


7 Implementace vyhled�v�n� pod�et�zce v �et�zci
Metoda vyhled�v�n�, kterou bude vyu��vat vestav�n� funkce find:
b) Pro vyhled�v�n� pou�ijte Boyer-Moore�v algoritmus (libovoln� typ heuristiky).

8 Implementace �azen�
Metoda �azen�, kterou bude vyu��vat vestav�n� funkce sort:
1) Pro �azen� pou�ijte algoritmus Quick sort.

9 Implementace tabulky symbol�
Tabulka symbol� bude implementov�na pomoc� abstraktn� datov� struktury:
II) Tabulku symbol� implementujte pomoc� hashovac� tabulky.    */

#ifndef IAL_H_
#define IAL_H_

#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include "str.h"
#include "main.h"
#define TABLEMAX     20     //  po�et polozek pole ukazatelu
#define SEM_OK        0
#define SEM_ERROR     3

typedef enum{Tundef,Tnum,Tstr,Tbool}Ttype;

typedef struct TableItem {
  Ttype type; 	//typ
  string name; 	//n�zev
  union {
    double Vnum;
    string Vstr;
    bool Vbool;
  } value;
  bool declar;		//deklarov�no
  bool used;   		//pou�ito
  struct TableItem* next;
}*tTableItem;

typedef struct SymbolTable {
  tTableItem items[TABLEMAX];
}tSymbolTable;

//hlavicky fci pro boyer-moore
int find (string *str, string *search);
void ascii_p(int ascii_pole[], char *str, int length);
void search_p(int search_pole[], char *search_str, int search_length);

//hlavicky fci pro quick sort
void quickSort(char *pole, int start, int konec);
string sort(string *sort);

//hlavicky fci pro tabulku symbolu
void initHT(tSymbolTable* table);
int hash(char* name);
int insertnewHT(tSymbolTable* table,  string *name, Ttype type);
void setStringValueHT(tTableItem table_item,string *val);
tTableItem findHT (tSymbolTable* table, char* name);
int removeItemHT (tSymbolTable* table, char* name);
void destroyHT (tSymbolTable* table);
void PrintHT( tSymbolTable* table );

#endif //IAL_H_
